﻿using BAL.Model.BaseOrder;
using Common.Util;
using DAL.DA;
using DAL.Model.Condition;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using NPOI.SS.UserModel;


namespace BAL
{
    public class OrderServices
    {

        OrderMaster_DA _OrderMaster_DAL = new OrderMaster_DA();
        OrderMasterGoods_DA _OrderMasterGoods_DA = new OrderMasterGoods_DA();
        OptionServices _optionServices = new OptionServices();

        /// <summary>
        /// 新增訂單
        /// </summary>
        /// <returns></returns>
        public string InsertOrder(OrderAddModel model)
        {
            var userid = SessionUtil.Current.UserId;
            DateTime dateTime = DateTime.Now;

            try
            {
                //取得訂單(暫時)
                string id = DateTime.Now.ToString("yyyyMMddHHmmssfff") + string.Format("{0:000}", (new Random()).Next(1000));

                //建立訂單主表
                OrderMaster_Condition orderMaster_Condition = new OrderMaster_Condition();
                orderMaster_Condition.OrderId = id;
                orderMaster_Condition.Quantity = model.OrderGoods.Count();
                orderMaster_Condition.ReceiveContact = model.ReceiveContact;
                orderMaster_Condition.ReceiveTel1 = model.ReceiveTel1;
                orderMaster_Condition.ReceiveTel2 = model.ReceiveTel2;
                orderMaster_Condition.ReceiveCity = model.ReceiveCity;
                orderMaster_Condition.ReceiveArea = model.ReceiveArea;
                orderMaster_Condition.ReceiveAddress = model.ReceiveAddress;
                orderMaster_Condition.SendContact = model.SendContact;
                orderMaster_Condition.SendTel1 = model.SendTel1;
                orderMaster_Condition.SendTel2 = model.SendTel2;
                orderMaster_Condition.SendCity = model.SendCity;
                orderMaster_Condition.SendArea = model.SendArea;
                orderMaster_Condition.SendAddress = model.SendAddress;
                orderMaster_Condition.PaymentMethod = model.PaymentMethod.Value;
                orderMaster_Condition.CollectionMoney = model.CollectionMoney == null ? 0 : model.CollectionMoney.Value;
                orderMaster_Condition.Arriveassigndate = model.Arriveassigndate;
                orderMaster_Condition.TimePeriod = model.TimePeriod.Value;
                orderMaster_Condition.InvoiceDesc = model.InvoiceDesc;
                orderMaster_Condition.ArticleNumber = model.ArticleNumber;
                orderMaster_Condition.SendPlatform = model.SendPlatform;
                orderMaster_Condition.ArticleName = model.ArticleName;
                orderMaster_Condition.DeliveryMethod = model.DeliveryMethod.Value;
                orderMaster_Condition.ReceiptFlag = model.ReceiptFlag;
                orderMaster_Condition.ExcelImport = model.ExcelImport;
                orderMaster_Condition.Status = "W";//等待中
                orderMaster_Condition.CreateUser = userid;
                orderMaster_Condition.UpdateUser = userid;
                orderMaster_Condition.CreateDate = dateTime;
                orderMaster_Condition.UpdateDate = dateTime;
                _OrderMaster_DAL.Insert(orderMaster_Condition);

                //建立訂單貨
                foreach (var OrderGood in model.OrderGoods)
                {
                    OrderMasterGoods_Condition orderMasterGoods_Condition = new OrderMasterGoods_Condition();
                    orderMasterGoods_Condition.OrderId = id;
                    orderMasterGoods_Condition.CbmSize = OrderGood.CbmSize.Value;
                    orderMasterGoods_Condition.CreateUser = userid;
                    orderMasterGoods_Condition.UpdateUser = userid;
                    orderMasterGoods_Condition.CreateDate = dateTime;
                    orderMasterGoods_Condition.UpdateDate = dateTime;
                    _OrderMasterGoods_DA.Insert(orderMasterGoods_Condition);
                }

                return id;

            }
            catch (Exception)
            {

                return string.Empty;
            }


        }

        /// <summary>
        /// 搜尋訂單
        /// </summary>
        /// <returns></returns>
        public List<OrderSearchTable> GetOrderTable(OrderSearchForm searchForm)
        {
            var GetPaymentMethodOption = _optionServices.GetPaymentMethodOption();
            var GetDeliveryMethodOption = _optionServices.GetDeliveryMethodOption();
            var GetTimePeriodOption = _optionServices.GetTimePeriodOption();
            var GetOrderMasterStatusOption = _optionServices.GetOrderMasterStatusOption();

            List<OrderSearchTable> orderSearchTables = new List<OrderSearchTable>();
            var Infos = _OrderMaster_DAL.Search(searchForm);
            if (Infos != null)
            {
                foreach (var item in Infos)
                {
                    OrderSearchTable orderSearchTable = new OrderSearchTable();
                    orderSearchTable.OrderId = item.OrderId;
                    orderSearchTable.Quantity = item.Quantity;
                    orderSearchTable.ReceiveContact = item.ReceiveContact;
                    orderSearchTable.ReceiveTel1 = item.ReceiveTel1;
                    orderSearchTable.ReceiveAddress = item.ReceiveAddress;
                    orderSearchTable.SendContact = item.SendContact;
                    orderSearchTable.SendTel1 = item.SendTel1;
                    orderSearchTable.SendAddress = item.SendAddress;
                    orderSearchTable.PaymentMethod = GetPaymentMethodOption.Where(x => x.value.Equals(item.PaymentMethod.ToString())).First().label;
                    orderSearchTable.CollectionMoney = item.CollectionMoney;
                    orderSearchTable.Arriveassigndate = item.Arriveassigndate == null ? string.Empty : item.Arriveassigndate.Value.ToString("yyyy/MM/dd");
                    orderSearchTable.TimePeriod = GetTimePeriodOption.Where(x => x.value.Equals(item.TimePeriod.ToString())).First().label;
                    orderSearchTable.DeliveryMethod = GetDeliveryMethodOption.Where(x => x.value.Equals(item.DeliveryMethod.ToString())).First().label;
                    orderSearchTable.ExcelImport = item.ExcelImport;
                    orderSearchTable.Status = GetOrderMasterStatusOption.Where(x => x.value.Equals(item.Status)).First().label;
                    orderSearchTable.CreateDate = item.CreateDate.ToString("yyyy/MM/dd HH:mm:ss");
                    orderSearchTable.UpdateDate = item.UpdateDate.ToString("yyyy/MM/dd HH:mm:ss");
                    orderSearchTable.CreateUser = item.CreateUser;
                    orderSearchTable.UpdateUser = item.UpdateUser;
                    orderSearchTables.Add(orderSearchTable);
                }
            }
            return orderSearchTables;
        }


        /// <summary>
        /// 批次訂單
        /// </summary>
        /// <returns></returns>
        public OrderBatchUploadModel BatchUpload(HttpPostedFileBase file)
        {

            var excelname = DateTime.Now.ToString("yyyyMMddHHmmss") + Guid.NewGuid().ToString("N").Substring(0, 6);

            OrderBatchUploadModel orderBatchUploadModel = new OrderBatchUploadModel();
            orderBatchUploadModel.ErrorMessage = string.Empty;
            orderBatchUploadModel.ExcelImport = excelname;

            var GetPaymentMethodOption = _optionServices.GetPaymentMethodOption();
            var GetDeliveryMethodOption = _optionServices.GetDeliveryMethodOption();
            var GetTimePeriodOption = _optionServices.GetTimePeriodOption();
            var GetOrderMasterStatusOption = _optionServices.GetOrderMasterStatusOption();
            var GetCbmSizeOption = _optionServices.GetCbmSizeOption();
            var GetCityOption = _optionServices.GetCityOption();


            int SuccessCount = 0;
            int FailCount = 0;
            string ErrorMessage = string.Empty;

            //分析檔案
            try
            {
                bool f = false;//是否有失敗

                XSSFWorkbook workbook = new XSSFWorkbook(file.InputStream); ;
                XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);
                for (int i = 3; i <= sheet.LastRowNum; i++)
                {
                    XSSFRow row = (XSSFRow)sheet.GetRow(i);

                    //一個一個處理
                    OrderAddModel _orderAddModel = new OrderAddModel();

                    _orderAddModel.ExcelImport = excelname;




                    List<OrderGood> _orderGoods = new List<OrderGood>();
                    OrderGood _orderGood = new OrderGood();
                    //收件人
                    try
                    {
                        var ReceiveContact = row.GetCell(0) == null ? string.Empty : row.GetCell(0).ToString();

                        if (ReceiveContact.Length > 50)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ReceiveContact = ReceiveContact;

                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人 : " + e.Message + "  ";
                        f = true;
                    }


                    //收件人電話1
                    try
                    {
                        var ReceiveTel1 = row.GetCell(1) == null ? string.Empty : row.GetCell(1).ToString();

                        if (ReceiveTel1.Length > 20)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ReceiveTel1 = ReceiveTel1;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人電話1 : " + e.Message + "  ";
                        f = true;
                    }


                    //收件人電話2

                    try
                    {
                        var ReceiveTel2 = row.GetCell(2) == null ? string.Empty : row.GetCell(2).ToString();

                        if (ReceiveTel2.Length > 20)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ReceiveTel2 = ReceiveTel2;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人電話2 : " + e.Message + "  ";
                        f = true;
                    }


                    //收件人城市

                    try
                    {
                        var ReceiveCity = row.GetCell(3) == null ? string.Empty : row.GetCell(3).ToString();

                        var Info = GetCityOption.Where(x => x.label.Equals(ReceiveCity)).FirstOrDefault();

                        if (Info == null)
                        {
                            throw new Exception("查無");
                        }
                        _orderAddModel.ReceiveCity = ReceiveCity;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人城市 : " + e.Message + "  ";
                        f = true;
                    }
                    //收件人地區

                    try
                    {
                        var ReceiveArea = row.GetCell(4) == null ? string.Empty : row.GetCell(4).ToString();

                        if (!string.IsNullOrEmpty(_orderAddModel.ReceiveCity)) //如果城市沒有值 一定有錯 可以跳過
                        {
                           var Info = _optionServices.GetAreaOption(_orderAddModel.ReceiveCity).Where(x => x.label.Equals(ReceiveArea)).FirstOrDefault();

                            if (Info == null)
                            {
                                throw new Exception("查無");
                            }
                            _orderAddModel.ReceiveArea = ReceiveArea;
                        }

                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人地區 : " + e.Message + "  ";
                        f = true;
                    }

                    //收件人地址

                    try
                    {
                        var ReceiveAddress = row.GetCell(5) == null ? string.Empty : row.GetCell(5).ToString();

                        if (ReceiveAddress.Length > 300)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ReceiveAddress = ReceiveAddress;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "收件人地址 : " + e.Message + "  ";
                        f = true;
                    }


                    //訂單類別
                    try
                    {
                        var DeliveryMethod = row.GetCell(6) == null ? string.Empty : row.GetCell(6).ToString();

                        var Info = GetDeliveryMethodOption.Where(x => x.label.Equals(DeliveryMethod)).FirstOrDefault();

                        if (Info == null)
                        {
                            throw new Exception("查無");
                        }
                        _orderAddModel.DeliveryMethod = int.Parse(Info.value);
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "訂單類別 : " + e.Message + "  ";
                        f = true;
                    }

                    //付款方式
                    try
                    {
                        var PaymentMethod = row.GetCell(7) == null ? string.Empty : row.GetCell(7).ToString();

                        var Info = GetPaymentMethodOption.Where(x => x.label.Equals(PaymentMethod)).FirstOrDefault();

                        if (Info == null)
                        {
                            throw new Exception("查無");
                        }
                        _orderAddModel.PaymentMethod = int.Parse(Info.value);
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "付款方式 : " + e.Message + "  ";
                        f = true;
                    }

                    //代收貨款
                    try
                    {
                        var CollectionMoney = row.GetCell(8) == null ? string.Empty : row.GetCell(8).ToString();

                        var c = int.TryParse(CollectionMoney, out int _collectionMoney);

                        if (c)
                        {
                            if (_collectionMoney > 20000)
                            {
                                throw new Exception("超過20000");
                            }
                        }
                        else
                        {
                            throw new Exception("有誤");
                        }
                        _orderAddModel.CollectionMoney = _collectionMoney;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "代收貨款 : " + e.Message + "  ";
                        f = true;
                    }
                    //派送時段
                    try
                    {
                        var TimePeriod = row.GetCell(9) == null ? string.Empty : row.GetCell(9).ToString();

                        var Info = GetTimePeriodOption.Where(x => x.label.Equals(TimePeriod)).FirstOrDefault();

                        if (Info == null)
                        {
                            throw new Exception("查無");
                        }
                        _orderAddModel.TimePeriod = int.Parse(Info.value);
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "派送時段 : " + e.Message + "  ";
                        f = true;
                    }

                    //產品編號

                    try
                    {
                        var ArticleNumber = row.GetCell(10) == null ? string.Empty : row.GetCell(10).ToString();

                        if (ArticleNumber.Length > 20)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ArticleNumber = ArticleNumber;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "產品編號 : " + e.Message + "  ";
                        f = true;
                    }
                    //出貨平台

                    try
                    {
                        var SendPlatform = row.GetCell(11) == null ? string.Empty : row.GetCell(11).ToString();

                        if (SendPlatform.Length > 20)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.SendPlatform = SendPlatform;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "出貨平台 : " + e.Message + "  ";
                        f = true;
                    }
                    //品名

                    try
                    {
                        var ArticleName = row.GetCell(12) == null ? string.Empty : row.GetCell(12).ToString();

                        if (ArticleName.Length > 20)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.ArticleName = ArticleName;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "品名 : " + e.Message + "  ";
                        f = true;
                    }
                    //備註

                    try
                    {
                        var InvoiceDesc = row.GetCell(13) == null ? string.Empty : row.GetCell(13).ToString();

                        if (InvoiceDesc.Length > 200)
                        {
                            throw new Exception("字數超過");
                        }
                        _orderAddModel.InvoiceDesc = InvoiceDesc;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "備註 : " + e.Message + "  ";
                        f = true;
                    }
                    //預計抵達日

                    try
                    {
                        var Arriveassigndate = row.GetCell(14) == null ? string.Empty : row.GetCell(14).ToString();

                        var d = DateTime.TryParse(Arriveassigndate, out DateTime _arriveassigndate);

                        if (!d)
                        {
                            throw new Exception("有誤");
                        }
                        _orderAddModel.Arriveassigndate = _arriveassigndate;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "預計抵達日 : " + e.Message + "  ";
                        f = true;
                    }
                    //回單

                    try
                    {
                        var ReceiptFlag = row.GetCell(15) == null ? string.Empty : row.GetCell(15).ToString();
                        //只有是跟否
                        bool _receiptFlag = true;
                        bool d = true;
                        if (ReceiptFlag.Equals("是"))
                        {
                            _receiptFlag = true;
                            d = true;
                        }
                        else if (ReceiptFlag.Equals("否"))
                        {
                            _receiptFlag = true;
                            d = true;
                        }
                        else
                        {
                            d = false;
                        }
                        if (!d)
                        {
                            throw new Exception("有誤");
                        }
                        _orderAddModel.ReceiptFlag = _receiptFlag;
                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "回單 : " + e.Message + "  ";
                        f = true;
                    }

                    //尺寸
                    try
                    {
                        var CbmSize = row.GetCell(16) == null ? string.Empty : row.GetCell(16).ToString();

                        var Info = GetCbmSizeOption.Where(x => x.label.Equals(CbmSize)).FirstOrDefault();

                        if (Info == null)
                        {
                            throw new Exception("查無");
                        }
                        _orderGood.CbmSize = int.Parse(Info.value);
                        _orderGoods.Add(_orderGood);
                        _orderAddModel.OrderGoods = _orderGoods;

                    }
                    catch (Exception e)
                    {
                        ErrorMessage = ErrorMessage + "尺寸 : " + e.Message + "  ";
                        f = true;
                    }

                    //解析完取資料
                    if (f) //有失敗資料
                    {
                        FailCount++;
                        ErrorMessage = "第" + i + "行->" + ErrorMessage + "<br/>";

                        if (orderBatchUploadModel.ErrorMessage.Length < 500) 
                        {
                            orderBatchUploadModel.ErrorMessage += ErrorMessage;

                            if (orderBatchUploadModel.ErrorMessage.Length >= 500) 
                            {
                                orderBatchUploadModel.ErrorMessage = orderBatchUploadModel.ErrorMessage + "錯誤太多 請確認格式....";
                            }

                        }
                        ErrorMessage = string.Empty;
                    }
                    else
                    {
        
                        //寄件人先死
                        _orderAddModel.SendContact = "小小明";
                        _orderAddModel.SendTel1 = "0933456789";
                        _orderAddModel.SendCity = "台北市";
                        _orderAddModel.SendArea = "中正區";
                        _orderAddModel.SendAddress = "羅斯福路三段1號";
                        InsertOrder(_orderAddModel);

                        SuccessCount++;
                    }

                }
                //結果
                orderBatchUploadModel.SuccessCount = SuccessCount;
                orderBatchUploadModel.FailCount = FailCount;
               
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());

            }
            return orderBatchUploadModel;
        }


    }
}
