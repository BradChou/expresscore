﻿using DAL.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DAL.Model.BasicModel;

namespace BAL
{
    public class OptionServices
    {
        ItemCodes_DA _ItemCodes_DA = new ItemCodes_DA();
        BS_AREA_DA _BS_AREA_DA = new BS_AREA_DA();
        public List<Option> GetPaymentMethodOption()
        {
            return _ItemCodes_DA.GetInfo("PaymentMethod").Select(x => new Option { label = x.CodeName, value = x.CodeId }).ToList();
        }
        public List<Option> GetTimePeriodOption()
        {
            return _ItemCodes_DA.GetInfo("TimePeriod").Select(x => new Option { label = x.CodeName, value = x.CodeId }).ToList();
        }
        public List<Option> GetDeliveryMethodOption()
        {
            return _ItemCodes_DA.GetInfo("DeliveryMethod").Select(x => new Option { label = x.CodeName, value = x.CodeId }).ToList();
        }
        public List<Option> GetCbmSizeOption()
        {
            return _ItemCodes_DA.GetInfo("CbmSize").Select(x => new Option { label = x.CodeName, value = x.CodeId }).ToList();
        }
        public List<Option> GetOrderMasterStatusOption()
        {
            return _ItemCodes_DA.GetInfo("OrderMasterStatus").Select(x => new Option { label = x.CodeName, value = x.CodeId }).ToList();
        }
        public List<Option> GetCityOption()
        {
            return _BS_AREA_DA.GetCityInfos().Select(x => new Option { label = x.AREA_SECT1, value = x.AREA_SECT1 }).ToList();
        }
        public List<Option> GetAreaOption(string City)
        {
            var Area = _BS_AREA_DA.GetAreaInfos(City);

            if (Area == null || Area.Count()==0) 
            {
                return null;
            }
            else 
            {
                return Area.Select(x => new Option { label = x.AREA_SECT2, value = x.AREA_SECT2 }).ToList();
            }
        }

    }
}
