﻿using Common.Util;
using Dapper;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.BaseWordModel;

namespace DAL
{
    public class BaseWordDao : BasicDao
    {
        public List<Table> GetHeaderTableData(string FUNC_ID)
        {
            string sqlQry = @"SELECT * FROM BS_WORDS
                    WHERE FUNC_ID LIKE @FUNC_ID AND HEAD_FG='Y'";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                object param = new
                {
                    FUNC_ID = "%" + FUNC_ID + "%"
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }

        public List<Table> GetWordTableData(string FUNC_ID)
        {
            string sqlQry = @"SELECT * FROM BS_WORDS
                    WHERE FUNC_ID = @FUNC_ID AND ISNULL(HEAD_FG,'')<>'Y'";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                object param = new
                {
                    FUNC_ID = FUNC_ID
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }

        /// <summary>
        /// 新增字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            //header的條件
            string sqlCnt = @"SELECT COUNT(*) AS CNT FROM BS_WORDS WHERE FUNC_ID=@FUNC_ID";
            //word的條件
            string sqlCnt_word = @"SELECT COUNT(*) AS CNT FROM BS_WORDS WHERE CMN_WORD=@CMN_WORD AND FUNC_ID=@FUNC_ID";
            //新增
            string sqlIn = @"INSERT INTO BS_WORDS(FUNC_ID,FUNC_DESC,CMN_WORD,DSP_ORD,HEAD_FG,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME)
                    VALUES(@FUNC_ID,@FUNC_DESC,@CMN_WORD,@DSP_ORD,@HEAD_FG,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    FUNC_ID = form.FUNC_ID,
                    FUNC_DESC = form.FUNC_DESC,
                    CMN_WORD = form.CMN_WORD,
                    DSP_ORD = form.DSP_ORD,
                    HEAD_FG = form.HEAD_FG,
                    CREATE_USER = SessionUtil.Current.UserId
                };
                if (form.HEAD_FG.Equals("Y"))
                {
                    if (conn.QuerySingle(sqlCnt, param).CNT > 0) result = -2;
                    else result = conn.Execute(sqlIn, param);
                }
                else
                {
                    if (conn.QuerySingle(sqlCnt_word, param).CNT > 0) result = -2;
                    else result = conn.Execute(sqlIn, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 編輯字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            //word的條件
            string sqlCnt_word = @"SELECT COUNT(*) AS CNT FROM BS_WORDS WHERE CMN_WORD=@CMN_WORD AND FUNC_ID=@FUNC_ID AND WD_SEQ<>@WD_SEQ";
            //新增
            string sqlIn = @"UPDATE BS_WORDS SET FUNC_ID=@FUNC_ID,FUNC_DESC=@FUNC_DESC,CMN_WORD=@CMN_WORD
                    ,DSP_ORD=@DSP_ORD,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE()
                    WHERE WD_SEQ=@WD_SEQ";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    WD_SEQ = form.WD_SEQ,
                    FUNC_ID = form.FUNC_ID,
                    FUNC_DESC = form.FUNC_DESC,
                    CMN_WORD = form.CMN_WORD,
                    DSP_ORD = form.DSP_ORD,
                    HEAD_FG = form.HEAD_FG,
                    EDIT_USER = SessionUtil.Current.UserId
                };
                if (form.HEAD_FG.Equals("Y"))
                {
                    result = conn.Execute(sqlIn, param);
                }
                else
                {
                    if (conn.QuerySingle(sqlCnt_word, param).CNT > 0) result = -2;
                    else result = conn.Execute(sqlIn, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 刪除字典資料
        /// </summary>
        /// <param name="WD_SEQ"></param>
        /// <param name="HEAD_FG"></param>
        /// <returns></returns>
        public int Delete(string WD_SEQ,string HEAD_FG)
        {
            string sqlDel = @"DELETE FROM BS_WORDS WHERE WD_SEQ=@WD_SEQ";
            //刪除該header和底下所有項目
            string sqlDel_header = @"DELETE FROM BS_WORDS WHERE FUNC_ID=(SELECT FUNC_ID FROM BS_WORDS WHERE WD_SEQ=@WD_SEQ)";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    WD_SEQ = WD_SEQ
                };
                if (HEAD_FG.Equals("Y"))
                {
                    result = conn.Execute(sqlDel_header, param);
                }
                else
                {
                    result = conn.Execute(sqlDel, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }
    }
}