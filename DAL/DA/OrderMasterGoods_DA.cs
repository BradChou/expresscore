﻿using Common.Util;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class OrderMasterGoods_DA : BasicDao
    {
        /// <summary>
        /// Insert  OrderMasterGoods
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public void Insert(OrderMasterGoods_Condition data)
        {
            string sqlQry = @"
INSERT INTO [dbo].[OrderMasterGoods]
           ([OrderId]
           ,[CbmSize]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[Weight]
           ,[CreateDate]
           ,[UpdateDate]
           ,[CreateUser]
           ,[UpdateUser])
     VALUES
            (@OrderId
           ,@CbmSize
           ,@Length
           ,@Width
           ,@Height
           ,@Weight
           ,@CreateDate
           ,@UpdateDate
           ,@CreateUser
           ,@UpdateUser)
";

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    conn.Execute(sqlQry, data);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }

            }
        }
    }
}
