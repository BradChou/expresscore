﻿using Common.Util;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public class BS_AREA_DA : BasicDao
    {
        /// <summary>
        /// 取得縣市列表
        /// </summary>
        /// <returns></returns>
        public List<BS_AREA_Condition> GetCityInfos()
        {
            string sqlQry = @"SELECT DISTINCT AREA_SECT1,AREA_SECT1 FROM BS_AREA WITH (NOLOCK)";

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    return conn.Query<BS_AREA_Condition> (sqlQry).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return null;
            }
        }
        /// <summary>
        /// 取得鄉鎮市區列表
        /// </summary>
        /// <returns></returns>
        public List<BS_AREA_Condition> GetAreaInfos(string AREA_SECT1)
        {
            string sqlQry = @"SELECT DISTINCT AREA_SECT2,AREA_SECT2  FROM BS_AREA WITH (NOLOCK) WHERE AREA_SECT1 = @AREA_SECT1";
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    return conn.Query<BS_AREA_Condition>(sqlQry,new { AREA_SECT1 }).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return null;
            }
        }
    }
}
