﻿using BAL.Model.BaseOrder;
using Common.Util;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
    public  class OrderMaster_DA : BasicDao
    {
        /// <summary>
        /// Insert OrderMaster
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public void Insert(OrderMaster_Condition data)
        {
            string sqlQry = @"
INSERT INTO [dbo].[OrderMaster]
           ([OrderId]
           ,[Quantity]
           ,[ReceiveContact]
           ,[ReceiveTel1]
           ,[ReceiveTel2]
           ,[ReceiveCity]
           ,[ReceiveArea]
           ,[ReceiveAddress]
           ,[SendContact]
           ,[SendTel1]
           ,[SendTel2]
           ,[SendCity]
           ,[SendArea]
           ,[SendAddress]
           ,[PaymentMethod]
           ,[CollectionMoney]
           ,[Arriveassigndate]
           ,[TimePeriod]
           ,[InvoiceDesc]
           ,[ArticleNumber]
           ,[SendPlatform]
           ,[ArticleName]
           ,[DeliveryMethod]
           ,[ReceiptFlag]
           ,[ExcelImport]
           ,[Status]
           ,[CreateDate]
           ,[UpdateDate]
           ,[CreateUser]
           ,[UpdateUser])
     VALUES
           (@OrderId
           ,@Quantity
           ,@ReceiveContact
           ,@ReceiveTel1
           ,@ReceiveTel2
           ,@ReceiveCity
           ,@ReceiveArea
           ,@ReceiveAddress
           ,@SendContact
           ,@SendTel1
           ,@SendTel2
           ,@SendCity
           ,@SendArea
           ,@SendAddress
           ,@PaymentMethod
           ,@CollectionMoney
           ,@Arriveassigndate
           ,@TimePeriod
           ,@InvoiceDesc
           ,@ArticleNumber
           ,@SendPlatform
           ,@ArticleName
           ,@DeliveryMethod
           ,@ReceiptFlag
           ,@ExcelImport
           ,@Status
           ,@CreateDate
           ,@UpdateDate
           ,@CreateUser
           ,@UpdateUser)

";

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                     conn.Execute(sqlQry, data);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
              
            }
        }

        /// <summary>
        /// Search OrderMaster
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public List<OrderMaster_Condition> Search(OrderSearchForm searchForm)
        {
            string sqlQry = @"
SELECT *
  FROM [dbo].[OrderMaster]
WHERE (@OrderId is NULL OR OrderId=@OrderId) 

AND (@ExcelImport is NULL OR ExcelImport = @ExcelImport)
";

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                   return conn.Query<OrderMaster_Condition>(sqlQry, searchForm).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return null;
            }
        }


    }
}
