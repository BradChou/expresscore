﻿using Common.Util;
using DAL.Model.Condition;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DA
{
 public   class ItemCodes_DA : BasicDao
    {
        /// <summary>
        /// Get ItemCodes
        /// </summary>
        /// <returns></returns>
        public List<ItemCodes_Condition> GetInfo(string CodeType, string SystemType="OrderSystem")
        {
            string sqlQry = @"
SELECT *
  FROM [dbo].[ItemCodes] WITH (NOLOCK)
 WHERE CodeType = @CodeType AND SystemType = @SystemType
ORDER BY OrderBy
";

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    return conn.Query<ItemCodes_Condition>(sqlQry,new { CodeType, SystemType }).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return null;
            }
        }
    }
}
