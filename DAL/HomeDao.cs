﻿using Common.Util;
using Dapper;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.HomeModel;

namespace DAL
{
    public class HomeDao : BasicDao
    {
        /// <summary>
        /// 取得功能選單樹狀結構
        /// </summary>
        /// <returns></returns>
        public List<Menu> GetMenu(string RootID, string Ftype)
        {
            string sqlQry = string.Format(@"EXEC SHOW_USER_FUNC_MENU '{0}','{1}','{2}'", SessionUtil.Current.UserId, RootID, Ftype);

            SqlConnection conn = GetConnection();
            List<Menu> result = new List<Menu>();

            try
            {
                result = conn.Query<Menu>(sqlQry).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }
    }
}