﻿using Common.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.SysSetModel;

namespace DAL
{
    public class SysSetDao : BasicDao
    {
        public List<Table> GetTableData(string SET_DESC)
        {
            string sqlQry = @"SELECT * FROM SYS_SET
                    WHERE SET_DESC LIKE @SET_DESC";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                object param = new
                {
                    SET_DESC = "%" + SET_DESC + "%"
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }

        /// <summary>
        /// 新增系統參數
        /// </summary>
        /// <param name="form"></param>
        /// <returns>-1:error|-2:重複ID</returns>
        public int Add(Form form)
        {
            string sqlCnt = @"SELECT COUNT(*) AS CNT FROM SYS_SET WHERE SET_ID=@SET_ID";
            string sqlIn = @"INSERT INTO SYS_SET
                    VALUES(@SET_ID,@SET_DESC,@SET_VALUE,@REMARKS,@DSP_ORD,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    SET_ID = form.SET_ID,
                    SET_DESC = form.SET_DESC,
                    SET_VALUE = form.SET_VALUE,
                    REMARKS = form.REMARKS,
                    DSP_ORD = form.DSP_ORD,
                    CREATE_USER = SessionUtil.Current.UserId
                };
                if (conn.QuerySingle(sqlCnt, param).CNT > 0)
                {
                    result = -2;
                }
                else
                {
                    result = conn.Execute(sqlIn, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            return result;
        }
        
        /// <summary>
        /// 編輯系統參數
        /// </summary>
        /// <param name="form"></param>
        /// <returns>-1:error</returns>
        public int Edit(Form form)
        {
            string sqlIn = @"UPDATE SYS_SET SET 
                    SET_DESC=@SET_DESC,SET_VALUE=@SET_VALUE,REMARKS=@REMARKS,DSP_ORD=@DSP_ORD
                    ,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE() WHERE SET_ID=@SET_ID";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    SET_ID = form.SET_ID,
                    SET_DESC = form.SET_DESC,
                    SET_VALUE = form.SET_VALUE,
                    REMARKS = form.REMARKS,
                    DSP_ORD = form.DSP_ORD,
                    EDIT_USER = SessionUtil.Current.UserId
                };
                result = conn.Execute(sqlIn, param);
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            return result;
        }

    }
}