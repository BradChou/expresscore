﻿using Common.Util;
using Dapper;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DAL
{
    public class BasicDao
    {
        protected string ConnectStr;
        public BasicDao()
        {
            ConnectStr = ConfigurationManager.ConnectionStrings["DATABASE"].ConnectionString;
        }
        /// <summary>
        /// 功能使用權限
        /// </summary>
        public class Auth
        {
            public string CAN_RUN { get; set; }
            public string CAN_SEARCH { get; set; }
            public string CAN_ADD { get; set; }
            public string CAN_MODIFY { get; set; }
            public string CAN_DEL { get; set; }
            public string CAN_PRINT { get; set; }
        }

        /// <summary>
        /// 取得資料庫連線
        /// </summary>
        /// <returns></returns>
        protected SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(ConnectStr);
            if (conn.State != ConnectionState.Open) conn.Open();

            return conn;
        }
        
        /// <summary>
        /// 取得該功能可使用權限
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public Auth GetAuth(string controller ,string FUNC_TYPE="Order")
        {
            string sqlQry = @"SELECT CAN_RUN,CAN_SEARCH,CAN_ADD,CAN_MODIFY,CAN_DEL,CAN_PRINT
                              FROM USER_FUNC_DETAIL
                              WHERE USER_ID = @USER_ID
                                AND FUNC_ID = (SELECT FUNC_ID FROM SYS_FUNC WHERE URL_ADDR = @URL_ADDR AND FUNC_TYPE =@FUNC_TYPE)
";

            using(SqlConnection conn = GetConnection())
            {
                Auth result = new Auth();
                try
                {
                    result = conn.QuerySingleOrDefault<Auth>(sqlQry, new { URL_ADDR = controller
                        , USER_ID = SessionUtil.Current.UserId ,
                        FUNC_TYPE= FUNC_TYPE
                    });
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }
    }
}