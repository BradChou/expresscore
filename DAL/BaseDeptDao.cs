﻿using Common.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.BaseDeptModel;
using static DAL.Model.BasicModel;

namespace DAL
{
    public class BaseDeptDao:BasicDao
    {
        /// <summary>
        /// 取得下拉選項
        /// </summary>
        /// <param name="FUNC_ID">功能ID</param>
        /// <returns></returns>
        public List<Option> GetOption(string FUNC_ID)
        {
            string sqlQry = @"SELECT CMN_WORD AS value , CMN_WORD AS label
                                FROM BS_WORDS
                                WHERE FUNC_ID = @FUNC_ID AND CMN_WORD IS NOT NULL";

            SqlConnection conn = GetConnection();
            List<Option> result = new List<Option>();

            try
            {
                result = conn.Query<Option>(sqlQry,new { FUNC_ID=FUNC_ID}).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }

            conn.Close();
            return result;
        }
        
        /// <summary>
        /// 取得部門資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public List<Form> GetTableData(SearchForm data)
        {
            string sqlQry = @"SELECT DEPT_ID,DEPT_NAME,DEPT_BRF,DEPT_TYPE
                                ,TEL_NO,CONTACT,DEPT_ADDR,POS_LONG,POS_LATI,ACT_FG
                                ,DEPT_LEVEL,DEPT_STN,B_AREA,AREA
                              FROM BS_DEPT
                              WHERE DEPT_ID LIKE @DEPT_ID AND DEPT_NAME LIKE @DEPT_NAME
                                AND DEPT_TYPE LIKE @DEPT_TYPE AND DEPT_LEVEL LIKE @DEPT_LEVEL
                                AND DEPT_STN LIKE @DEPT_STN AND AREA LIKE @AREA AND ACT_FG LIKE @ACT_FG";

            SqlConnection conn = GetConnection();
            List<Form> result = new List<Form>();
            try
            {
                object param = new
                {
                    DEPT_ID = "%" + data.id + "%",
                    DEPT_NAME = "%" + data.name + "%",
                    DEPT_TYPE = "%" + data.typeValue + "%",
                    DEPT_LEVEL = "%" + data.levelValue + "%",
                    DEPT_STN = "%" + data.stnValue + "%",
                    AREA = "%" + data.areaValue + "%",
                    ACT_FG = "%" + data.actValue + "%"
                };
                result = conn.Query<Form>(sqlQry, param).ToList();
            }
            catch(Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 新增部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            string sqlIn = @"INSERT INTO BS_DEPT
                                (DEPT_ID,DEPT_NAME,DEPT_BRF,DEPT_TYPE,TEL_NO,CONTACT
                                ,DEPT_ADDR,POS_LONG,POS_LATI,DEPT_LEVEL,DEPT_STN,B_AREA,AREA
                                ,ACT_FG,CREATE_USER,CREATE_TIME)
                            VALUES 
                                (@DEPT_ID,@DEPT_NAME,@DEPT_BRF,@DEPT_TYPE,@TEL_NO,@CONTACT
                                ,@DEPT_ADDR,@POS_LONG,@POS_LATI,@DEPT_LEVEL,@DEPT_STN,@B_AREA
                                ,@AREA,@ACT_FG,@CREATE_USER,GETDATE())";

            using (SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    form.CREATE_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, form);
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 修改部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            string sqlIn = @"UPDATE BS_DEPT SET
                                DEPT_NAME = @DEPT_NAME,
                                DEPT_BRF = @DEPT_BRF,
                                DEPT_TYPE = @DEPT_TYPE,
                                TEL_NO = @TEL_NO,
                                CONTACT = @CONTACT,
                                DEPT_ADDR = @DEPT_ADDR,
                                POS_LONG = @POS_LONG,
                                POS_LATI = @POS_LATI,
                                DEPT_LEVEL = @DEPT_LEVEL,
                                DEPT_STN = @DEPT_STN,
                                B_AREA = @B_AREA,
                                AREA = @AREA,
                                ACT_FG = @ACT_FG,
                                EDIT_USER = @EDIT_USER,
                                EDIT_TIME = GETDATE()
                              WHERE DEPT_ID = @DEPT_ID";

            using (SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    form.EDIT_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, form);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }
    }
}