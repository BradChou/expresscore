﻿using Common.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.BasicModel;
using static DAL.Model.SysUserModel;

namespace DAL
{
    public class SysUserDao :BasicDao
    {
        /// <summary>
        /// 取得部門選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetDeptOption()
        {
            string sqlQry = @"SELECT DEPT_ID AS value,DEPT_NAME AS label FROM BS_DEPT WHERE ACT_FG = 'Y'";

            List<Option> result = new List<Option>();
            using(SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得角色選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetRoleOption()
        {
            string sqlQry = @"SELECT ROLE_ID AS value,ROLE_NAME AS label 
                                FROM BS_ROLE WHERE ACT_FG = 'Y'";

            List<Option> result = new List<Option>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得職位選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetJobTitleOption()
        {
            string sqlQry = @"SELECT CMN_WORD AS value,CMN_WORD AS label 
                                FROM BS_WORDS WHERE FUNC_ID = 'bsJobTitle' AND HEAD_FG IS NULL";

            List<Option> result = new List<Option>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<Form> GetTableData(SearchForm form)
        {
            string sqlQry = @"SELECT USER_ID,USER_NAME,USER_TYPE,DEPART_ID
                                ,JOB_TITLE,USER_SEX,TEL_NO,CELL_PHONE,TRANS_TYPE
                                ,TRANS_CODE,EMAIL,ACT_FG,ACT_DATE,VOID_DATE
                                ,EXPIRE_DATE,REMARKS
                                ,(SELECT ROLE_ID + ',' FROM SYS_USER_ROLE WHERE USER_ID = SU.USER_ID FOR XML PATH('')) AS ROLE_ID
                              FROM SYS_USER SU
                              WHERE USER_ID LIKE @USER_ID AND USER_NAME LIKE @USER_NAME
                                AND DEPART_ID LIKE @DEPART_ID AND USER_TYPE LIKE @USER_TYPE
                                AND ACT_FG LIKE @ACT_FG";
            List<Form> result = new List<Form>();
            using(SqlConnection conn = GetConnection())
            {
                try
                {
                    object param = new
                    {
                        USER_ID = "%" + form.id + "%",
                        USER_NAME = "%" + form.name + "%",
                        DEPART_ID = "%" + form.dept + "%",
                        USER_TYPE = "%" + form.type + "%",
                        ACT_FG = "%" + form.act + "%"
                    };
                    result = conn.Query<Form>(sqlQry, param).ToList();
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 新增使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            string sqlInSU = @"INSERT INTO SYS_USER
                                (USER_ID,USER_NAME,USER_PWD,USER_TYPE,DEPART_ID,JOB_TITLE
                                ,USER_SEX,EMAIL,TEL_NO,CELL_PHONE,ACT_FG,ACT_DATE,VOID_DATE
                                ,EXPIRE_DATE,REMARKS,CREATE_USER,CREATE_TIME)
                             VALUES
                                (@USER_ID,@USER_NAME,@USER_PWD,@USER_TYPE,@DEPART_ID,@JOB_TITLE
                                ,@USER_SEX,@EMAIL,@TEL_NO,@CELL_PHONE,@ACT_FG,@ACT_DATE,@VOID_DATE
                                ,@EXPIRE_DATE,@REMARKS,@CREATE_USER,GETDATE())";

            string sqlInSUR = @"INSERT INTO SYS_USER_ROLE
                                    (USER_ID,ROLE_ID,CREATE_USER,CREATE_TIME)
                                VALUES
                                    (@USER_ID,@ROLE_ID,@CREATE_USER,GETDATE())";

            int result;
            using(SqlConnection conn = GetConnection())
            {
                try
                {
                    form.CREATE_USER = SessionUtil.Current.UserId;
                    form.USER_PWD = EncryUtil.Md5(form.USER_PWD);
                    List<object> param = new List<object>();
                    foreach(string ROLE_ID in form.ROLE_ID.Split(','))
                    {
                        param.Add(new {
                            USER_ID = form.USER_ID,
                            ROLE_ID = ROLE_ID,
                            CREATE_USER = form.CREATE_USER
                        });
                    }
                    using (SqlTransaction tran = conn.BeginTransaction())
                    {
                        result = conn.Execute(sqlInSU, form, transaction: tran);
                        result += conn.Execute(sqlInSUR, param, transaction: tran);
                        tran.Commit();
                    }
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 修改使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            string sqlUpSU = @"UPDATE SYS_USER SET
                                USER_NAME = @USER_NAME,
                                USER_TYPE = @USER_TYPE,DEPART_ID = @DEPART_ID,
                                JOB_TITLE = @JOB_TITLE,USER_SEX = @USER_SEX,
                                EMAIL = @EMAIL,TEL_NO = @TEL_NO,
                                CELL_PHONE = @CELL_PHONE,ACT_FG = @ACT_FG,
                                ACT_DATE = @ACT_DATE,VOID_DATE = @VOID_DATE,
                                EXPIRE_DATE = @EXPIRE_DATE,REMARKS = @REMARKS,
                                EDIT_USER = @EDIT_USER,EDIT_TIME = GETDATE()
                               WHERE USER_ID = @USER_ID";

            string sqlUpSUR = @"IF NOT EXISTS (SELECT * FROM SYS_USER_ROLE WHERE USER_ID = @USER_ID AND ROLE_ID = @ROLE_ID) 
                                BEGIN
                                    INSERT INTO SYS_USER_ROLE
                                        (USER_ID,ROLE_ID,CREATE_USER,CREATE_TIME)
                                    VALUES
                                        (@USER_ID,@ROLE_ID,@CREATE_USER,GETDATE())
                                END";
            string sqlDelSUR = @"DELETE FROM SYS_USER_ROLE WHERE USER_ID = @USER_ID AND ROLE_ID NOT IN @ROLE_IDS";
            int result;
            using(SqlConnection conn = GetConnection())
            {
                try
                {
                    form.CREATE_USER = SessionUtil.Current.UserId;
                    string[] roleIds = form.ROLE_ID.Split(',');
                    List<object> upParam = new List<object>();
                    foreach(string roleId in roleIds)
                    {
                        upParam.Add(new
                        {
                            ROLE_ID = roleId,
                            USER_ID = form.USER_ID,
                            CREATE_USER = form.CREATE_USER
                        });
                    }
                    object delParam = new
                    {
                        USER_ID = form.USER_ID,
                        ROLE_IDS = roleIds
                    };
                    using (SqlTransaction tran = conn.BeginTransaction())
                    {
                        result = conn.Execute(sqlUpSU, form, transaction: tran);
                        //執行運算式會回傳-1不加入result
                        conn.Execute(sqlUpSUR, upParam, transaction: tran);
                        result += conn.Execute(sqlDelSUR, delParam, transaction: tran);
                        tran.Commit();
                    }
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 刪除使用者資料
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <returns></returns>
        public int Del(string USER_ID)
        {
            string sqlDelSU = @"DELETE FROM SYS_USER WHERE USER_ID = @USER_ID";
            string sqlDelSUR = @"DELETE FROM SYS_USER_ROLE WHERE USER_ID = @USER_ID";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Execute(sqlDelSU, new { USER_ID = USER_ID });
                    result += conn.Execute(sqlDelSUR, new { USER_ID = USER_ID });
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }
    }
}