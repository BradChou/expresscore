﻿using Common.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.BasicModel;
using static DAL.Model.OrderSearchModel;

namespace DAL
{
    public class OrderSearchDao :BasicDao
    {
        /// <summary>
        /// 取得部門選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetDeptOption()
        {
            string sqlQry = @"SELECT DEPT_ID AS value,DEPT_NAME AS label FROM BS_DEPT WHERE ACT_FG = 'Y'";

            List<Option> result = new List<Option>();
            using(SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得角色選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetRoleOption()
        {
            string sqlQry = @"SELECT ROLE_ID AS value,ROLE_NAME AS label 
                                FROM BS_ROLE WHERE ACT_FG = 'Y'";

            List<Option> result = new List<Option>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得職位選項
        /// </summary>
        /// <returns></returns>
        public List<Option> GetJobTitleOption()
        {
            string sqlQry = @"SELECT CMN_WORD AS value,CMN_WORD AS label 
                                FROM BS_WORDS WHERE FUNC_ID = 'bsJobTitle' AND HEAD_FG IS NULL";

            List<Option> result = new List<Option>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<Form> GetTableData(SearchForm form)
        {
            string sqlQry = @" 
 if @StartDate is not null and @EndDate is not null 
 Begin
 select
 D.DeliveryId,
 O.OrderId, 
 Convert(varchar,O.CreateDate, 120) as CreateDate,
 O.SendContact,
 O.ReceiveContact,
 O.ReceiveAddress,
 O.Quantity,
 O.CollectionMoney,
 Convert(varchar,O.Arriveassigndate, 120) as Arriveassigndate,
 O.DeliveryMethod,
 Case O.Status When 'S' Then '成功' 
 When 'F' Then '失敗'
 When 'P' Then '處理中'
 When 'W' Then '等待中'
 When 'C' Then '失敗'
 End Status
 from OrderMaster O with(nolock)
 Left join DeliveryMaster D with(nolock) on D.OrderId = O.OrderId
 Where O.Status like @Status
 and O.DeliveryMethod like @DeliveryMethod
 and O.OrderId like @OrderId
 and O.CreateDate >= @StartDate
 and (O.CreateDate < Dateadd(dd,1, @EndDate))
 and ISNULL(D.DeliveryId,'') like @DeliveryId
 end 
 
 else
 Begin
select
 D.DeliveryId,
 O.OrderId, 
 Convert(varchar,O.CreateDate, 120) as CreateDate,
 O.SendContact,
 O.ReceiveContact,
 O.ReceiveAddress,
 O.Quantity,
 O.CollectionMoney,
 Convert(varchar,O.Arriveassigndate, 120) as Arriveassigndate,
 O.DeliveryMethod,
 Case O.Status When 'S' Then '成功' 
 When 'F' Then '失敗'
 When 'P' Then '處理中'
 When 'W' Then '等待中'
 When 'C' Then '失敗'
 End Status
 from OrderMaster O with(nolock)
 Left join DeliveryMaster D with(nolock) on D.OrderId = O.OrderId
 Where O.Status like @Status
 and O.DeliveryMethod like @DeliveryMethod
 and O.OrderId like @OrderId
 and ISNULL(D.DeliveryId,'') like @DeliveryId 
 end";
            List<Form> result = new List<Form>();
            using(SqlConnection conn = GetConnection())
            {
                try
                {    
                   object param = new
                   {
                            OrderId = "%" + form.OrderId + "%",
                            Status = "%" + form.Status + "%",
                            DeliveryMethod = "%" + form.DeliveryMethod + "%",
                            StartDate = form.StartDate,
                            EndDate = form.EndDate,
                            DeliveryId = "%" + form.DeliveryId + "%",
                   };
                    result = conn.Query<Form>(sqlQry, param).ToList();                          
                }                    
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }
        /// <summary>
        /// 修改使用者資料
        /// </summary>
        /// <param name = "form" ></ param >
        /// < returns ></ returns >
        public int Edit(Form form)
        {
            string sqlUpSU = @"UPDATE OrderMaster SET
        CreateDate = @CreateDate,
		SendContact= @SendContact,
		ReceiveContact = @ReceiveContact,
		ReceiveAddress = @ReceiveAddress,
		Quantity = @Quantity,
		CollectionMoney = @CollectionMoney,
		Arriveassigndate = @Arriveassigndate,
        UpdateDate =  GETDATE(),
        UpdateUser = @UpdateUser
        WHERE OrderId = @OrderId";

            //string sqlUpSUR = @"IF NOT EXISTS (SELECT * FROM SYS_USER_ROLE WHERE USER_ID = @USER_ID AND ROLE_ID = @ROLE_ID) 
            //                    BEGIN
            //                        INSERT INTO SYS_USER_ROLE
            //                            (USER_ID,ROLE_ID,CREATE_USER,CREATE_TIME)
            //                        VALUES
            //                            (@USER_ID,@ROLE_ID,@CREATE_USER,GETDATE())
            //                    END";
            //string sqlDelSUR = @"DELETE FROM SYS_USER_ROLE WHERE USER_ID = @USER_ID AND ROLE_ID NOT IN @ROLE_IDS";
            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    //form.CREATE_USER = SessionUtil.Current.UserId;
                    //string[] roleIds = form.ROLE_ID.Split(',');
                    //List<object> upParam = new List<object>();
                    form.UpdateUser = SessionUtil.Current.UserId;
                    object upParam = new
                    {
                        UpdateUser = form.UpdateUser,
                        SendContact = form.SendContact,
                        ReceiveContact = form.SendContact,
                        ReceiveAddress = form.ReceiveAddress,
                        Quantity = form.Quantity,
                        CollectionMoney = form.CollectionMoney,
                        Arriveassigndate =form.Arriveassigndate
         
                    };
                    //foreach (string roleId in roleIds)
                    //{
                    //    upParam.Add(new
                    //    {
                    //        ROLE_ID = roleId,
                    //        USER_ID = form.USER_ID,
                    //        CREATE_USER = form.CREATE_USER
                    //    });
                    //}
                    //object delParam = new
                    //{
                    //    USER_ID = form.USER_ID,
                    //    ROLE_IDS = roleIds
                    //};
                    using (SqlTransaction tran = conn.BeginTransaction())
                    {
                        result = conn.Execute(sqlUpSU, form, transaction: tran);
                        //執行運算式會回傳-1不加入result
                        //conn.Execute(sqlUpSUR, upParam, transaction: tran);
                        //result += conn.Execute(sqlDelSUR, delParam, transaction: tran);
                        tran.Commit();
                    }
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }
    }
}