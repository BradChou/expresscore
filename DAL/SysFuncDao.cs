﻿using Common.Util;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.SysFuncModel;

namespace DAL
{
    public class SysFuncDao:BasicDao
    {
        public List<Table> GetTableData(string FUNC_ID,string FUNC_DESC)
        {
            string sqlQry = @"SELECT FUNC_ID,FUNC_DESC,PARENT_ID,FUNC_TYPE,URL_ADDR,REMARKS
                                ,CASE SET_RUN WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_RUN
                                ,CASE SET_SEARCH WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_SEARCH
                                ,CASE SET_ADD WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_ADD
                                ,CASE SET_MODIFY WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_MODIFY
                                ,CASE SET_DEL WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_DEL
                                ,CASE SET_PRINT WHEN 'Y' THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS SET_PRINT
                              FROM SYS_FUNC
                              WHERE FUNC_ID LIKE @FUNC_ID AND FUNC_DESC LIKE @FUNC_DESC";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                object param = new
                {
                    FUNC_ID = "%"+FUNC_ID+"%",
                    FUNC_DESC = "%"+FUNC_DESC+"%"
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }catch(Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 新增系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            string sqlIn = @"INSERT INTO SYS_FUNC 
                                (FUNC_ID,FUNC_DESC,PARENT_ID,FUNC_TYPE,URL_ADDR,REMARKS,SET_RUN,SET_SEARCH,SET_ADD,SET_MODIFY,SET_DEL,SET_PRINT,CREATE_USER,CREATE_TIME)
                             VALUES
                                (@FUNC_ID,@FUNC_DESC,@PARENT_ID,@FUNC_TYPE,@URL_ADDR,@REMARKS,@SET_RUN,@SET_SEARCH,@SET_ADD,@SET_MODIFY,@SET_DEL,@SET_PRINT,@CREATE_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    FUNC_ID = form.FUNC_ID,
                    FUNC_DESC = form.FUNC_DESC,
                    PARENT_ID = form.PARENT_ID,
                    FUNC_TYPE = form.FUNC_TYPE,
                    URL_ADDR = form.URL_ADDR,
                    REMARKS = form.REMARKS,
                    SET_RUN = form.SET_RUN,
                    SET_SEARCH = form.SET_SEARCH,
                    SET_ADD = form.SET_ADD,
                    SET_MODIFY = form.SET_MODIFY,
                    SET_DEL = form.SET_DEL,
                    SET_PRINT = form.SET_PRINT,
                    CREATE_USER = SessionUtil.Current.UserId
                };
                result = conn.Execute(sqlIn, param);
            }catch(Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 編輯系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            string sqlUp = @"UPDATE SYS_FUNC SET 
                                FUNC_DESC = @FUNC_DESC,
                                PARENT_ID = @PARENT_ID,
                                FUNC_TYPE = @FUNC_TYPE,
                                URL_ADDR = @URL_ADDR,
                                REMARKS = @REMARKS,
                                SET_RUN = @SET_RUN,
                                SET_SEARCH = @SET_SEARCH,
                                SET_ADD = @SET_ADD,
                                SET_MODIFY = @SET_MODIFY,
                                SET_DEL = @SET_DEL,
                                SET_PRINT = @SET_PRINT,
                                EDIT_USER = @EDIT_USER,
                                EDIT_TIME = GETDATE()
                            WHERE FUNC_ID = @FUNC_ID";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    FUNC_ID = form.FUNC_ID,
                    FUNC_DESC = form.FUNC_DESC,
                    PARENT_ID = form.PARENT_ID,
                    FUNC_TYPE = form.FUNC_TYPE,
                    URL_ADDR = form.URL_ADDR,
                    REMARKS = form.REMARKS,
                    SET_RUN = form.SET_RUN,
                    SET_SEARCH = form.SET_SEARCH,
                    SET_ADD = form.SET_ADD,
                    SET_MODIFY = form.SET_MODIFY,
                    SET_DEL = form.SET_DEL,
                    SET_PRINT = form.SET_PRINT,
                    EDIT_USER = SessionUtil.Current.UserId
                };
                result = conn.Execute(sqlUp, param);
            }catch(Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }
    }
}