﻿using Common.Util;
using Dapper;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static DAL.Model.BaseCalModel;

namespace DAL
{
    public class BaseCalDao : BasicDao
    {
        /// <summary>
        /// 取得行事曆資料(前一、目前、後一個月
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public List<Calender> GetCalenderData(string Date)
        {
            string sqlQry = @"SELECT MONTH(CAL_DATE) AS MON,DAY(CAL_DATE) AS DAY,CAL_DESC 
                    FROM BS_CAL 
                    WHERE ACT_FG='Y' AND CAL_DATE >=DATEADD(MONTH,-1, @DATE)
                    AND CAL_DATE < DATEADD(MONTH,2, @DATE)";
            SqlConnection conn = GetConnection();
            List<Calender> result = new List<Calender>();
            try
            {
                DateTime dt = DateTime.Parse(Date);
                object param = new
                {
                    DATE = dt.ToString("yyyyMMdd")
                };
                var sqlData = conn.Query(sqlQry, param).ToList();
                foreach (var data in sqlData)
                {
                    Calender item = new Calender();
                    item.months = new string[] { data.MON.ToString("00") };
                    item.days = new string[] { data.DAY.ToString("00") };
                    item.desc = data.CAL_DESC;
                    result.Add(item);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 取得當月行事曆資料
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public List<Table> GetTableData(string Date)
        {
            string sqlQry = @"SELECT CAL_SEQ,CONVERT(VARCHAR,CAL_DATE,120) AS CAL_DATE,DAY(CAL_DATE) AS DAY
                    ,CAL_DESC,REST_FG,FIX_FG,REMARKS,ACT_FG
                    FROM BS_CAL 
                    WHERE YEAR(CAL_DATE)=@YEAR AND MONTH(CAL_DATE)=@MONTH";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                DateTime dt = DateTime.Parse(Date);
                object param = new
                {
                    YEAR = dt.Year,
                    MONTH = dt.Month
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 取得現在年度所有行事曆資料
        /// </summary>
        /// <returns></returns>
        public List<Table> GetTableData()
        {
            string sqlQry = @"SELECT CAL_SEQ,CONVERT(VARCHAR,CAL_DATE,111) AS CAL_DATE
                    ,CAL_DESC,REST_FG,FIX_FG,REMARKS,ACT_FG,1 as 'CK_FG'
                    FROM BS_CAL 
                    WHERE FIX_FG='Y' AND YEAR(CAL_DATE)=YEAR(GETDATE())";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                result = conn.Query<Table>(sqlQry).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }
        
        /// <summary>
        /// 新增行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            //前面傳過來的CAL_DATE會固定少8小時
            string sqlIn = @"INSERT INTO BS_CAL (CAL_DATE,CAL_DESC,REST_FG,FIX_FG,REMARKS,ACT_FG,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME)
                    VALUES(DATEADD(HOUR,8,CONVERT(DATETIME,@CAL_DATE)),@CAL_DESC,@REST_FG,@FIX_FG,@REMARKS,@ACT_FG,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    CAL_DATE = form.CAL_DATE,
                    CAL_DESC = form.CAL_DESC,
                    REST_FG = form.REST_FG,
                    FIX_FG = form.FIX_FG,
                    REMARKS = form.REMARKS,
                    ACT_FG = form.ACT_FG,
                    CREATE_USER = SessionUtil.Current.UserId
                };
                result = conn.Execute(sqlIn, param);
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 編輯行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            //前面傳過來的CAL_DATE會固定少8小時
            string sqlUp = @"UPDATE BS_CAL SET
                    CAL_DATE=DATEADD(HOUR,8,CONVERT(DATETIME,@CAL_DATE))
                    ,CAL_DESC=@CAL_DESC,REST_FG=@REST_FG,FIX_FG=@FIX_FG,REMARKS=@REMARKS
                    ,ACT_FG=@ACT_FG,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE()
                    WHERE CAL_SEQ=@CAL_SEQ";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    CAL_SEQ = form.CAL_SEQ,
                    CAL_DATE = form.CAL_DATE,
                    CAL_DESC = form.CAL_DESC,
                    REST_FG = form.REST_FG,
                    FIX_FG = form.FIX_FG,
                    REMARKS = form.REMARKS,
                    ACT_FG = form.ACT_FG,
                    EDIT_USER = SessionUtil.Current.UserId
                };
                result = conn.Execute(sqlUp, param);
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 刪除行事曆資料
        /// </summary>
        /// <param name="CAL_SEQ"></param>
        /// <returns></returns>
        public int Delete(string CAL_SEQ)
        {
            string sqlDel = @"DELETE FROM BS_CAL WHERE CAL_SEQ=@CAL_SEQ";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    CAL_SEQ = CAL_SEQ
                };
                result = conn.Execute(sqlDel, param);
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 複製目前年度指定資料至指定年度
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public int Copy(string Year, List<Form> list)
        {
            string sqlDel = @"DELETE FROM BS_CAL WHERE YEAR(CAL_DATE)=@YEAR";
            string sqlIn = @"INSERT INTO BS_CAL (CAL_DATE,CAL_DESC,REST_FG,FIX_FG,REMARKS,ACT_FG,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME)
                    SELECT CONVERT(DATETIME,@YEAR+'/'+CONVERT(VARCHAR,MONTH(CAL_DATE))+'/'+CONVERT(VARCHAR,DAY(CAL_DATE)))
                    ,CAL_DESC,REST_FG,FIX_FG,REMARKS,ACT_FG,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE()
                    FROM BS_CAL WHERE CAL_SEQ IN @CAL_SEQ";
            SqlConnection conn = GetConnection();
            int result;
            using (var tran = conn.BeginTransaction())
            {
                try
                {
                    int[] seq = list.Where(a => a.CK_FG).Select(a => a.CAL_SEQ).ToArray();
                    conn.Execute(sqlDel, new { YEAR = Year }, transaction: tran);
                    result = conn.Execute(sqlIn, new { YEAR = Year, CAL_SEQ = seq, CREATE_USER = SessionUtil.Current.UserId }, transaction: tran);
                    tran.Commit();
                }
                catch(Exception e)
                {
                    tran.Rollback();
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
            }
            conn.Close();
            return result;
        }

        /// <summary>
        /// 確認該年度是否已有行事曆資料
        /// </summary>
        /// <param name="Year"></param>
        /// <returns></returns>
        public bool CheckYear(string Year)
        {
            string sqlQry = @"SELECT COUNT(*) AS CNT FROM BS_CAL WHERE YEAR(CAL_DATE) = @YEAR";
            SqlConnection conn = GetConnection();
            bool result = false;
            try
            {
                object param = new
                {
                    YEAR = Year
                };
                if (conn.QuerySingleOrDefault(sqlQry, param).CNT > 0) result = true;
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = false;
            }
            conn.Close();
            return result;
        }
    }
}