﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.Model.ReturnOrder;

namespace OrderSystem.Controllers
{
    public class ReturnOrderController : Controller
    {
        // GET: BaseOrder
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ADD(ReturnAddModel model)
        {
            return RedirectToAction("Index", "BaseOrder");
        }
    }
}