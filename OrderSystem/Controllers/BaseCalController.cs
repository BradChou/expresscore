﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.BaseCalModel;

namespace OrderSystem.Controllers
{
    public class BaseCalController : Controller
    {
        private BaseCalDao dao = new BaseCalDao();
        // GET: BaseCal
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得當月行事曆資料
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public JsonResult GetMainTableData(string DATE)
        {
            return Json(dao.GetTableData(DATE), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCopyTableData()
        {
            return Json(dao.GetTableData(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得行事曆資料(前一、目前、後一個月
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public JsonResult GetCalenderData(string Date)
        {
            return Json(dao.GetCalenderData(Date), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }

        /// <summary>
        /// 編輯行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }

        /// <summary>
        /// 刪除行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Delete(string CAL_SEQ)
        {
            return dao.Delete(CAL_SEQ);
        }

        public int Copy(string Year, bool Confirm, List<Form> data)
        {
            if (!dao.CheckYear(Year) || Confirm)
            {
                return dao.Copy(Year, data);
            }
            else
            {
                return -2;
            }
        }

    }
}