﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.BaseDeptModel;

namespace OrderSystem.Controllers
{
    public class BaseDeptController : Controller
    {
        private BaseDeptDao dao = new BaseDeptDao();
        // GET: BaseDept
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得下拉選項
        /// </summary>
        /// <param name="FUNC_ID"></param>
        /// <returns></returns>
        public JsonResult GetOption(string FUNC_ID)
        {
            return Json(dao.GetOption(FUNC_ID), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得部門資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult GetTableData(SearchForm data)
        {
            return Json(dao.GetTableData(data), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }

        /// <summary>
        /// 修改部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }
    }
}