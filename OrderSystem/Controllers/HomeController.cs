﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.HomeModel;

namespace OrderSystem.Controllers
{
    public class HomeController : Controller
    {

        private HomeDao dao = new HomeDao();
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 取得樹狀選單畫面
        /// </summary>
        /// <returns></returns>
        public ActionResult Menu()
        {
            string RootID = "FseOrderSystem";
            string Ftype = "Order";
            List<Menu> model = dao.GetMenu(RootID, Ftype);
            return PartialView(model);
        }
    }
}