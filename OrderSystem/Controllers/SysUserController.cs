﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.SysUserModel;

namespace OrderSystem.Controllers
{
    public class SysUserController : Controller
    {
        private SysUserDao dao = new SysUserDao();
        // GET: SysUser
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得部門選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDeptOption()
        {
            return Json(dao.GetDeptOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得職位選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetJobTitleOption()
        {
            return Json(dao.GetJobTitleOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得角色選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRoleOption()
        {
            return Json(dao.GetRoleOption(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetTableData(SearchForm form)
        {
            return Json(dao.GetTableData(form), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }
        /// <summary>
        /// 修改使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }

        /// <summary>
        /// 刪除使用者資料
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <returns></returns>
        public int Del(string USER_ID)
        {
            return dao.Del(USER_ID);
        }
    }
    }