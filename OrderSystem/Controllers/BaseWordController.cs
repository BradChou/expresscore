﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.BaseWordModel;

namespace OrderSystem.Controllers
{
    public class BaseWordController : Controller
    {

        BaseWordDao dao = new BaseWordDao();
        // GET: BaseWord
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        public JsonResult GetHeaderTableData(string FUNC_ID)
        {
            return Json(dao.GetHeaderTableData(FUNC_ID), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetWordTableData(string FUNC_ID)
        {
            return Json(dao.GetWordTableData(FUNC_ID), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }
        /// <summary>
        /// 編輯字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }
        /// <summary>
        /// 刪除字典資料
        /// </summary>
        /// <param name="WD_SEQ"></param>
        /// <param name="HEAD_FG"></param>
        /// <returns></returns>
        public int Delete(string WD_SEQ, string HEAD_FG)
        {
            return dao.Delete(WD_SEQ, HEAD_FG);
        }
    }
}