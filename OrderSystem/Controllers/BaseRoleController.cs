﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.BaseRoleModel;

namespace OrderSystem.Controllers
{
    public class BaseRoleController : Controller
    {
        private BaseRoleDao dao = new BaseRoleDao();
        // GET: BSRole
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }
        public ActionResult Setting(string ROLE_ID, string ROLE_NAME)
        {
            ViewBag.Id = ROLE_ID;
            ViewBag.Name = ROLE_NAME;
            return View();
        }

        public JsonResult GetTableData(string ROLE_ID, string ROLE_NAME, string ACT_FG)
        {
            return Json(dao.GetTableData(ROLE_ID, ROLE_NAME, ACT_FG), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSettingTableData(string ROLE_ID)
        {

            string RootID = "FseOrderSystem";
            string Ftype = "Order";

            return Json(dao.GetSettingTableData(RootID, Ftype, ROLE_ID), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 新增角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }

        /// <summary>
        /// 編輯角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }

        /// <summary>
        /// 儲存編輯權限資料
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int SaveSetting(string ROLE_ID, List<SettingTable> data)
        {
            return dao.SaveSetting(ROLE_ID, data);
        }

        /// <summary>
        /// 複製權限資料
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Copy(string ROLE_ID, Form form)
        {
            return dao.Copy(ROLE_ID, form);
        }
    }
}