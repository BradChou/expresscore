﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.SysFuncModel;


namespace OrderSystem.Controllers
{
    public class SysFuncController : Controller
    {
        private SysFuncDao dao = new SysFuncDao();
        // GET: SysFunc
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得搜尋資料
        /// </summary>
        /// <param name="FUNC_ID"></param>
        /// <param name="FUNC_DESC"></param>
        /// <returns></returns>
        public JsonResult GetTableData(string FUNC_ID, string FUNC_DESC)
        {
            return Json(dao.GetTableData(FUNC_ID, FUNC_DESC), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 新增系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            return dao.Add(form);
        }

        /// <summary>
        /// 編輯系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }
    }
}