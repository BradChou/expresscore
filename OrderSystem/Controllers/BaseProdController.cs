﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.BaseProdModel;

namespace OrderSystem.Controllers
{
    public class BaseProdController : Controller
    {

        private BaseProdDao dao = new BaseProdDao();
        // GET: BaseProd
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult GetTableData(Form data)
        {
            return Json(dao.GetTableData(data), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Form data)
        {
            return dao.Add(data);
        }

        /// <summary>
        /// 修改產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Edit(Form data)
        {
            return dao.Edit(data);
        }

    }
}