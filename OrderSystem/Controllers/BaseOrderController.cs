﻿using BAL;
using BAL.Model.BaseOrder;
using DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Controllers
{
    public class BaseOrderController : Controller
    {
        OrderServices _orderServices = new OrderServices();
        OptionServices _optionServices = new OptionServices();
        SysSetDao dao = new SysSetDao();
        // GET: BaseOrder
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得縣市列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCityOption()
        {
            return Json(_optionServices.GetCityOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得鄉鎮市區列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAreaOption(string City)
        {
            return Json(_optionServices.GetAreaOption(City), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 取得付款方式選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetPaymentMethodOption()
        {
            return Json(_optionServices.GetPaymentMethodOption(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得派送時段選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTimePeriodOption()
        {
            return Json(_optionServices.GetTimePeriodOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得訂單類別選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDeliveryMethodOption()
        {
            return Json(_optionServices.GetDeliveryMethodOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得物品才積選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCbmSizeOption()
        {
            return Json(_optionServices.GetCbmSizeOption(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ADD(OrderAddModel model)
        {
            //所有參數錯誤訊息
            if (!ModelState.IsValid)
            {
                string error = string.Empty;
                foreach (var e in ModelState.Where(x => x.Value.Errors.Count > 0))
                {
                    error = error + e.Value.Errors[0].ErrorMessage + ";";
                }
                return Json(new { error = error });
            }

            //新增
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
              var id=  _orderServices.InsertOrder(model);
                scope.Complete();

                return Json(new { OrderId = id });
            }


        }

        /// <summary>
        /// 取得訂單列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTableData(OrderSearchForm searchForm)
        {

            DateTime dateTime = DateTime.Now;

            if (searchForm == null)
            {
                return Json(new { error = "" });
            }

            //if (string.IsNullOrEmpty(searchForm.OrderId))
            //{
            //    if (searchForm.StartDate == null)
            //    {
            //        return Json(new { error = "請輸入開始時間" });
            //    }
            //    else
            //    {
            //        searchForm.StartDate = searchForm.StartDate.Value.Date;
            //    }
            //    if (searchForm.EndDate == null)
            //    {
            //        return Json(new { error = "請輸入結束時間" });

            //    }
            //    else
            //    {
            //        searchForm.EndDate = searchForm.EndDate.Value.Date.AddDays(1);
            //    }

            //    string dateDiff = null;
            //    TimeSpan ts1 = new TimeSpan(searchForm.StartDate.Value.Ticks);
            //    TimeSpan ts2 = new TimeSpan(searchForm.EndDate.Value.Ticks);
            //    TimeSpan ts = ts1.Subtract(ts2).Duration();
            //    //顯示時間  
            //    dateDiff = ts.Days.ToString() + "天"
            //            + ts.Hours.ToString() + "小時"
            //            + ts.Minutes.ToString() + "分鐘"
            //            + ts.Seconds.ToString() + "秒";

            //    if (ts.Days > 30)
            //    {
            //        //TempData["message"] = "時間範圍超過一個月";
            //        return Json(new { error = "時間範圍超過30天" });
            //    }

            //}

            return Json(_orderServices.GetOrderTable(searchForm), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 匯入資料
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                if (file == null)
                {
                    return Json(new { error = "上傳失敗" });
                }

                // BatchUpload

                var info =  _orderServices.BatchUpload(file);


                return Json(info, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(new { error = "上傳失敗" });
            }

        }

        /// <summary>
        /// 下載空白訂單格式
        /// </summary>
        /// <returns></returns>
        public FileResult DownloadFile()
        {
            FileStream stream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Excel", "空白訂單格式.xlsx"), FileMode.Open, FileAccess.Read, FileShare.Read);
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "空白訂單格式.xlsx");
        }
    }
}