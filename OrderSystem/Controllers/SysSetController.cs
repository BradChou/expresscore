﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DAL.Model.SysSetModel;

namespace OrderSystem.Controllers
{
    public class SysSetController : Controller
    {
        SysSetDao dao = new SysSetDao();
        // GET: SysSet
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }
        public JsonResult GetTableData(string SET_DESC)
        {
            return Json(dao.GetTableData(SET_DESC), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 新增系統參數
        /// </summary>
        /// <param name="form"></param>
        public int Add(Form form)
        {
            return dao.Add(form);
        }

        /// <summary>
        /// 編輯系統參數
        /// </summary>
        /// <param name="form"></param>
        public int Edit(Form form)
        {
            return dao.Edit(form);
        }
    }
}