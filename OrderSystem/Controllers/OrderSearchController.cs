﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using static DAL.Model.OrderSearchModel;

namespace OrderSystem.Controllers
{
    public class OrderSearchController : Controller
    {
        private OrderSearchDao dao = new OrderSearchDao();
        // GET: OrderSearch
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 取得部門選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDeptOption()
        {
            return Json(dao.GetDeptOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得職位選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetJobTitleOption()
        {
            return Json(dao.GetJobTitleOption(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得角色選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRoleOption()
        {
            return Json(dao.GetRoleOption(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetTableData(SearchForm form)
        {
            return Json(dao.GetTableData(form), JsonRequestBehavior.AllowGet);
        }

        public int Edit(Form form)
        {
            return dao.Edit(form);
        }

    }
}
