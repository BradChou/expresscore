﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Models
{
    public class LoginModel
    {
        public class UserInfo
        {
            /// <summary>
            /// 登入者工號
            /// </summary>
            public string USER_ID { get; set; }
            /// <summary>
            /// 登入者姓名
            /// </summary>
            public string USER_NAME { get; set; }
            /// <summary>
            /// 角色編號
            /// </summary>
            public string ROLE_ID { get; set; }
            /// <summary>
            /// 此次連線的token
            /// </summary>
            public string token { get; set; }
        }
    }
}