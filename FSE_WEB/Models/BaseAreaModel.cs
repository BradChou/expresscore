﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Models
{
    public class BaseAreaModel :BasicModel
    {
        public class Form
        {
            /// <summary>
            /// 區域代號
            /// </summary>
            public string AREA_ID { get; set; }
            /// <summary>
            /// 縣市
            /// </summary>
            public string AREA_SECT1 { get; set; }
            /// <summary>
            /// 鄉鎮市區
            /// </summary>
            public string AREA_SECT2 { get; set; }
            /// <summary>
            /// 街/路/段
            /// </summary>
            public string AREA_SECT3 { get; set; }
            /// <summary>
            /// 號碼
            /// </summary>
            public string AREA_SECT4 { get; set; }
            /// <summary>
            /// 郵遞區號
            /// </summary>
            public string ZIP_CODE { get; set; }
            /// <summary>
            /// 偏遠地區
            /// </summary>
            public string REMOTE_FG { get; set; }
            /// <summary>
            /// 轉聯運否
            /// </summary>
            public string TRSF_FG { get; set; }
            /// <summary>
            /// 站所代號
            /// </summary>
            public string DEPT_ID { get; set; }
            /// <summary>
            /// 聯運供應商(站所名稱)
            /// </summary>
            public string DEPT_NAME { get; set; }
            /// <summary>
            /// SD碼
            /// </summary>
            public string SD_CODE { get; set; }
            /// <summary>
            /// MD碼
            /// </summary>
            public string MD_CODE { get; set; }
            /// <summary>
            /// 堆疊區
            /// </summary>
            public string STACK_AREA { get; set; }
            /// <summary>
            /// 創建者
            /// </summary>
            public string CREATE_USER { get; set; }
            /// <summary>
            /// 編輯者
            /// </summary>
            public string EDIT_USER { get; set; }
        }

        public class BatchForm
        {
            /// <summary>
            /// 批次修改ID陣列
            /// </summary>
            public List<string> AREA_ID { get; set; }
            /// <summary>
            /// 站所代號
            /// </summary>
            public string DEPT_ID { get; set; }
            /// <summary>
            /// SD碼
            /// </summary>
            public string SD_CODE { get; set; }
            /// <summary>
            /// MD碼
            /// </summary>
            public string MD_CODE { get; set; }
            /// <summary>
            /// 堆疊區
            /// </summary>
            public string STACK_AREA { get; set; }
        }
    }
}