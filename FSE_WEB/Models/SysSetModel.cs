﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Models
{
    public class SysSetModel
    {
        public class Table
        {
            /// <summary>
            /// 參數設定編號
            /// </summary>
            public string SET_ID { get; set; }
            /// <summary>
            /// 設定項目名稱(說明)
            /// </summary>
            public string SET_DESC { get; set; }
            /// <summary>
            /// 參數設定內容
            /// </summary>
            public string SET_VALUE { get; set; }
            /// <summary>
            /// 備註事項
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
        }
        public class Form
        {
            /// <summary>
            /// 參數設定編號
            /// </summary>
            public string SET_ID { get; set; }
            /// <summary>
            /// 設定項目名稱(說明)
            /// </summary>
            public string SET_DESC { get; set; }
            /// <summary>
            /// 參數設定內容
            /// </summary>
            public string SET_VALUE { get; set; }
            /// <summary>
            /// 備註事項
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
        }
    }
}