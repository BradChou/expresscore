﻿namespace FSE_WEB.Models
{
    public class CustSetModel
    {
        public class Form
        {
            /// <summary>
            /// 客戶代碼
            /// </summary>
            public string CUST_CODE { get; set; }
            /// <summary>
            /// 客戶統編
            /// </summary>
            public string CUST_ID { get; set; }
            /// <summary>
            /// 中文名稱
            /// </summary>
            public string CHI_NAME { get; set; }
            /// <summary>
            /// 中文簡稱
            /// </summary>
            public string BRF_NAME { get; set; }
            /// <summary>
            /// 縣市
            /// </summary>
            public string CHI_CITY { get; set; }
            /// <summary>
            /// 鄉鎮區(市)
            /// </summary>
            public string CHI_AREA { get; set; }
            /// <summary>
            /// 中文地址
            /// </summary>
            public string CHI_ADDR { get; set; }
            /// <summary>
            /// 發票-縣市
            /// </summary>
            public string INV_CITY { get; set; }
            /// <summary>
            /// 發票-鄉鎮市區
            /// </summary>
            public string INV_AREA { get; set; }
            /// <summary>
            /// 發票地址
            /// </summary>
            public string INV_ADDR { get; set; }
            /// <summary>
            /// 聯絡人
            /// </summary>
            public string CONTACT { get; set; }
            /// <summary>
            /// 電話1
            /// </summary>
            public string TEL_NO1 { get; set; }
            /// <summary>
            /// 電話2
            /// </summary>
            public string TEL_NO2 { get; set; }
            /// <summary>
            /// 傳真
            /// </summary>
            public string FAX_NO { get; set; }
            /// <summary>
            /// 集團代號
            /// </summary>
            public string CUST_GROUP { get; set; }
            /// <summary>
            /// 電子郵件清單
            /// </summary>
            public string EMAIL_LIST { get; set; }
            /// <summary>
            /// 銀行代號
            /// </summary>
            public string BANK_ID { get; set; }
            /// <summary>
            /// 銀行名稱
            /// </summary>
            public string BANK_NAME { get; set; }
            /// <summary>
            /// 銀行分行
            /// </summary>
            public string BANK_BRANCH { get; set; }
            /// <summary>
            /// 銀行帳號
            /// </summary>
            public string BANK_ACCT { get; set; }
            /// <summary>
            /// 帳戶名稱
            /// </summary>
            public string BANK_CUST { get; set; }
            /// <summary>
            /// 開發站所
            /// </summary>
            public string DVLP_DEPT { get; set; }
            /// <summary>
            /// 集貨站所
            /// </summary>
            public string CLT_DEPT { get; set; }
            /// <summary>
            /// 啟用/停用
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 備註說明
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 建檔人
            /// </summary>
            public string CREATE_USER { get; set; }
            /// <summary>
            /// 修改人
            /// </summary>
            public string EDIT_USER { get; set; }
        }

        public class FormProd
        {
            public string CUST_CODE { get; set; }
            public string PROD_ID { get; set; }
            public string PROD_NAME { get; set; }
            public string EDIT_USER { get; set; }
        }

        public class FormUser
        {
            public string CUST_CODE { get; set; }
            public string USER_CODE { get; set; }
            public string USER_NAME { get; set; }
            public string USER_PWD { get; set; }
            public string EMAIL { get; set; }
            public string TEL_NO { get; set; }
            public string CELL_PHONE { get; set; }
            public string ACT_FG { get; set; }
            public string REMARKS { get; set; }
            public string CREATE_USER { get; set; }
            public string EDIT_USER { get; set; }
        }

        public class FormAddr
        {
            public string PS_SEQ { get; set; }
            public string CUST_CODE { get; set; }
            public string PICKSEND_FG { get; set; }
            public string PICK_FG { get; set; }
            public string SEND_FG { get; set; }
            public string PS_CITY { get; set; }
            public string PS_AREA { get; set; }
            public string PS_ADDR { get; set; }
            public string PS_COMP { get; set; }
            public string CONTACT { get; set; }
            public string TEL_NO1 { get; set; }
            public string TEL_NO2 { get; set; }
            public string CLT_DEPT { get; set; }
            public string DEPT_NAME { get; set; }
            public string CREATE_USER { get; set; }
            public string EDIT_USER { get; set; }
        }
    }
}