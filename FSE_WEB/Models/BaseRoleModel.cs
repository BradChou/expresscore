﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Models
{
    public class BaseRoleModel
    {
        public class Table
        {
            /// <summary>
            /// tabulator ID
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 角色代碼
            /// </summary>
            public string ROLE_ID { get; set; }
            /// <summary>
            /// 角色名稱
            /// </summary>
            public string ROLE_NAME { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
            /// <summary>
            /// 備註說明
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 狀態(啟用/停用)
            /// </summary>
            public string ACT_FG { get; set; }
        }

        public class Form
        {
            /// <summary>
            /// 角色代碼
            /// </summary>
            public string ROLE_ID { get; set; }
            /// <summary>
            /// 角色名稱
            /// </summary>
            public string ROLE_NAME { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
            /// <summary>
            /// 備註說明
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 狀態(啟用/停用)
            /// </summary>
            public string ACT_FG { get; set; }
        }

        public class SettingTable
        {
            /// <summary>
            /// 父項目
            /// </summary>
            public string PARENT { get; set; }
            /// <summary>
            /// 功能ID
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能說明
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 可否執行(1:Y/0:N)
            /// </summary>
            public string CAN_RUN { get; set; }
            public string SET_RUN { get; set; }
            /// <summary>
            /// 可否查詢(1:Y/0:N)
            /// </summary>
            public string CAN_SEARCH { get; set; }
            public string SET_SEARCH { get; set; }
            /// <summary>
            /// 可否新增(1:Y/0:N)
            /// </summary>
            public string CAN_ADD { get; set; }
            public string SET_ADD { get; set; }
            /// <summary>
            /// 可否編輯(1:Y/0:N)
            /// </summary>
            public string CAN_MODIFY { get; set; }
            public string SET_MODIFY { get; set; }
            /// <summary>
            /// 可否作廢(1:Y/0:N)
            /// </summary>
            public string CAN_DEL { get; set; }
            public string SET_DEL { get; set; }
            /// <summary>
            /// 可否列印(1:Y/0:N)
            /// </summary>
            public string CAN_PRINT { get; set; }
            public string SET_PRINT { get; set; }
            /// <summary>
            /// 子項目個數
            /// </summary>
            public int subNum { get; set; }
            /// <summary>
            /// 子項目List
            /// </summary>
            public List<SettingTable> _children { get; set; }
        }

    }
}