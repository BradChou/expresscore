﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Models
{
    public class HomeModel
    {
        public class Menu
        {
            /// <summary>
            /// 功能編號
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能名稱
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 頁籤名稱
            /// </summary>
            public string TAB_TITLE { get; set; }
            /// <summary>
            /// 功能路徑
            /// </summary>
            public string URL_ADDR { get; set; }
            /// <summary>
            /// 功能路徑參數
            /// </summary>
            public string URL_PARAM { get; set; }
            /// <summary>
            /// 階層
            /// </summary>
            public string FUNC_LEVEL { get; set; }
        }
    }
}