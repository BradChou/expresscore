USE [master]
GO
/****** Object:  Database [FSE]    Script Date: 2022/1/10 上午 09:34:42 ******/
CREATE DATABASE [FSE]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FSE', FILENAME = N'C:\SQL_DB\FSE.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FSE_log', FILENAME = N'C:\SQL_DB\FSE_log.ldf' , SIZE = 270336KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FSE] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FSE].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FSE] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FSE] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FSE] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FSE] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FSE] SET ARITHABORT OFF 
GO
ALTER DATABASE [FSE] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FSE] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FSE] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FSE] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FSE] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FSE] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FSE] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FSE] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FSE] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FSE] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FSE] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FSE] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FSE] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FSE] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FSE] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FSE] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FSE] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FSE] SET RECOVERY FULL 
GO
ALTER DATABASE [FSE] SET  MULTI_USER 
GO
ALTER DATABASE [FSE] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FSE] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FSE] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FSE] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FSE] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FSE', N'ON'
GO
ALTER DATABASE [FSE] SET QUERY_STORE = OFF
GO
USE [FSE]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE [FSE] SET  READ_WRITE 
GO
