USE [FSE]
GO
/****** Object:  StoredProcedure [dbo].[SHOW_ROLE_FUNC_MENU]    Script Date: 2022/1/20 上午 10:01:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SHOW_ROLE_FUNC_MENU]
(
    @RootID varchar(50) = 'GOD', -- 預設值
    @Ftype varchar(10) = 'WEB',
	@RoleID varchar(10) = 'NONE'
)
AS
	BEGIN
		WITH node AS
		(
			SELECT FUNC_ID,PARENT_ID
			FROM SYS_FUNC
			WHERE PARENT_ID=@RootID
			UNION ALL
			SELECT tb1.FUNC_ID,tb1.PARENT_ID
			FROM SYS_FUNC TB1
			INNER JOIN node TB2
			ON TB1.PARENT_ID = TB2.FUNC_ID
		)
		--select * from node
		SELECT DISTINCT TA1.PARENT AS FUNC_ID,TA2.FUNC_DESC,TA2.FUNC_TYPE,TA2.TAB_TITLE,TA2.URL_ADDR,
		TA2.URL_PARAM,'PARENT' as FUNC_LEVEL
		FROM (SELECT tb1.FUNC_ID AS PARENT,TB2.FUNC_ID,TB2.FUNC_DESC,TB2.FUNC_TYPE,
		TB2.TAB_TITLE,TB2.URL_ADDR,TB2.URL_PARAM
		FROM node TB1
		INNER JOIN SYS_FUNC TB2 ON TB1.FUNC_ID = TB2.PARENT_ID
		LEFT JOIN (
			SELECT FUNC_ID,CHAR(max(ASCII(CAN_ADD))) AS CAN_ADD,CHAR(max(ASCII(CAN_DEL))) AS CAN_DEL,
			CHAR(max(ASCII(CAN_MODIFY))) AS CAN_MODIFY,CHAR(max(ASCII(CAN_PRINT))) AS CAN_PRINT,
			CHAR(max(ASCII(CAN_RUN))) AS CAN_RUN,CHAR(max(ASCII(CAN_SEARCH))) AS CAN_SEARCH
			FROM SYS_ROLE_FUNC
			WHERE Role_ID IN (@RoleID)
			GROUP BY FUNC_ID) TB4 ON TB4.FUNC_ID=TB2.FUNC_ID
		WHERE (FUNC_TYPE=@Ftype)
		AND (TB4.CAN_ADD='Y' OR TB4.CAN_DEL='Y' OR TB4.CAN_MODIFY='Y' OR
			TB4.CAN_PRINT='Y' OR TB4.CAN_RUN='Y' OR TB4.CAN_SEARCH='Y')) TA1
		INNER JOIN SYS_FUNC TA2 ON TA1.PARENT=TA2.FUNC_ID
		UNION 
		SELECT TB2.FUNC_ID,TB2.FUNC_DESC,TB2.FUNC_TYPE,
		TB2.TAB_TITLE,TB2.URL_ADDR,TB2.URL_PARAM,'NODE' as FUNC_LEVEL
		FROM node TB1
		INNER JOIN SYS_FUNC TB2 ON TB1.FUNC_ID = TB2.PARENT_ID
		LEFT JOIN (
			SELECT FUNC_ID,CHAR(max(ASCII(CAN_ADD))) AS CAN_ADD,CHAR(max(ASCII(CAN_DEL))) AS CAN_DEL,
			CHAR(max(ASCII(CAN_MODIFY))) AS CAN_MODIFY,CHAR(max(ASCII(CAN_PRINT))) AS CAN_PRINT,
			CHAR(max(ASCII(CAN_RUN))) AS CAN_RUN,CHAR(max(ASCII(CAN_SEARCH))) AS CAN_SEARCH
			FROM SYS_ROLE_FUNC
			WHERE Role_ID IN (@RoleID)
			GROUP BY FUNC_ID) TB4 ON TB4.FUNC_ID=TB2.FUNC_ID
		WHERE (FUNC_TYPE=@Ftype)
		AND (TB4.CAN_ADD='Y' OR TB4.CAN_DEL='Y' OR TB4.CAN_MODIFY='Y' OR
			TB4.CAN_PRINT='Y' OR TB4.CAN_RUN='Y' OR TB4.CAN_SEARCH='Y')
		ORDER BY FUNC_ID
	END 
GO
