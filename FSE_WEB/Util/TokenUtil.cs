﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSE_WEB.Util
{
    public class TokenUtil
    {
        /// <summary>
        /// 產生token userID+userName+roleId+登入時間(到毫秒 雜湊
        /// </summary>
        /// <returns></returns>
        public static string CreateToken()
        {
            string token = EncryUtil.Md5(SessionUtil.Current.UserId
                +SessionUtil.Current.UserName+SessionUtil.Current.RoleId
                +SessionUtil.Current.LoginTime);
            return token;
        }

        /// <summary>
        /// 檢查token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool VerifyToken(string[] accessToken)
        {
            if(accessToken != null)
            {
                string originToken = EncryUtil.Md5(SessionUtil.Current.UserId
                + SessionUtil.Current.UserName + SessionUtil.Current.RoleId
                + SessionUtil.Current.LoginTime);

                return accessToken.First().Equals(originToken);
            }
            else
            {
                return false;
            }
        }
    }
}