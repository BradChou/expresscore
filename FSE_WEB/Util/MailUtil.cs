﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace FSE_WEB.Util
{
    public class MailUtil
    {
        private static string sendMailAccount= ConfigurationManager.AppSettings["SendMailAccount"];
        private static int port= int.Parse(ConfigurationManager.AppSettings["MailPort"]);
        private static string mailServer= ConfigurationManager.AppSettings["MailServer"];
        private static string testFlag= ConfigurationManager.AppSettings["TestFlag"]; //測試模式 Y;N
        private static string TestMail= ConfigurationManager.AppSettings["TestMail"]; //測試帳號
        private static string TestCc= ConfigurationManager.AppSettings["TestCc"]; //測試 Cc 帳號
        
        /// <summary>
        /// 寄送mail
        /// </summary>
        /// <param name="to">要寄送到的email</param>
        /// <param name="cc">副本的email</param>
        /// <param name="subject">主旨</param>
        /// <param name="msg">內容 (字串型態)</param>
        /// <param name="edcId">寄送此EMAIL的key</param>
        /// <param name="status">寄此mail的狀態</param>
        /// <param name="attachfile">附件檔案</param>
        /// <returns>true:成功;false:失敗</returns>
        public static void SendMail(List<string> to, List<string> cc, string subject, string msg, string edcId = null, string status = null, string mailformat = "plain", string attachfile = null, string sendMailAcc = null)
        {
            if (!string.IsNullOrEmpty(sendMailAcc)) sendMailAccount = sendMailAcc;

            var message = new MimeMessage();
            message.From.Add(MailboxAddress.Parse(sendMailAccount));

            if (testFlag.Equals("Y"))
            {
                message.To.Add(MailboxAddress.Parse(TestMail));
                message.Cc.Add(MailboxAddress.Parse(TestCc));
            }
            else
            {
                foreach (string account in to)
                {
                    message.To.Add(MailboxAddress.Parse(account));
                }
                foreach (string account in cc)
                {
                    message.Cc.Add(MailboxAddress.Parse(account));
                }
            }

            message.Subject = subject;

            var body = new TextPart(mailformat)
            {
                Text = string.Format(msg)
            };

            //var attachment = new MimePart()
            //{
            //    Content = new MimeContent(File.OpenRead(attachfile), ContentEncoding.Default),
            //    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
            //    ContentTransferEncoding = ContentEncoding.Base64,
            //    FileName = Path.GetFileName(attachfile)
            //};

            // now create the multipart/mixed container to hold the message text and the
            // image attachment
            var multipart = new Multipart("mixed");
            multipart.Add(body);
            //multipart.Add(attachment);
            if (attachfile != null)
            {
                var attachment = new MimePart()
                {
                    Content = new MimeContent(File.OpenRead(attachfile), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(attachfile)
                };
                multipart.Add(attachment);
            }


            // now set the multipart/mixed as the message body
            message.Body = multipart;

            try
            {
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    //client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(mailServer, port, false);

                    // Note: only needed if the SMTP server requires authentication
                    //client.Authenticate("racertran168@gmail.com", "bjo4t6mp4gj ");

                    client.Send(message);
                    client.Disconnect(true);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }
}