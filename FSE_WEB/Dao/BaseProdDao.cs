﻿using Dapper;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.BaseProdModel;

namespace FSE_WEB.Dao
{
    public class BaseProdDao :BasicDao
    {
        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public List<Form> GetTableData(Form data)
        {
            string sqlQry = @"SELECT PROD_ID,PROD_NAME,PROD_TYPE,ACT_FG
                              FROM BS_PROD
                              WHERE PROD_ID LIKE @PROD_ID AND PROD_NAME LIKE @PROD_NAME AND ACT_FG LIKE @ACT_FG";

            using(SqlConnection conn = GetConnection())
            {
                List<Form> result = new List<Form>();
                try
                {
                    data.PROD_ID = "%" + data.PROD_ID + "%";
                    data.PROD_NAME = "%" + data.PROD_NAME + "%";
                    data.ACT_FG = "%" + data.ACT_FG + "%";
                    result = conn.Query<Form>(sqlQry,data).ToList();
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 新增產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Form data)
        {
            string sqlIn = @"INSERT INTO BS_PROD
                                (PROD_ID,PROD_NAME,PROD_TYPE,ACT_FG,CREATE_USER,CREATE_TIME)
                             VALUES
                                (@PROD_ID,@PROD_NAME,@PROD_TYPE,@ACT_FG,@CREATE_USER,GETDATE())";
            using(SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    data.CREATE_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, data);
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 修改產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Edit(Form data)
        {
            string sqlIn = @"UPDATE BS_PROD SET
                                PROD_NAME = @PROD_NAME,
                                PROD_TYPE = @PROD_TYPE,
                                ACT_FG = @ACT_FG,
                                EDIT_USER = @EDIT_USER,
                                EDIT_TIME = GETDATE()
                             WHERE PROD_ID = @PROD_ID";
            using (SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    data.EDIT_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, data);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }
    }
}