﻿using Dapper;
using FSE_WEB.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.BaseAreaModel;
using static FSE_WEB.Models.BasicModel;

namespace FSE_WEB.Dao
{
    public class BaseAreaDao:BasicDao
    {
        /// <summary>
        /// 取得縣市列表
        /// </summary>
        /// <returns></returns>
        public List<Option> GetSect1Option()
        {
            string sqlQry = @"SELECT DISTINCT AREA_SECT1 AS value,AREA_SECT1 AS label FROM BS_AREA";

            using(SqlConnection conn = GetConnection())
            {
                List<Option> result = new List<Option>();
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得鄉鎮市區列表
        /// </summary>
        /// <returns></returns>
        public List<Option> GetSect2Option(string AREA_SECT1)
        {
            string sqlQry = @"SELECT DISTINCT AREA_SECT2 AS value,AREA_SECT2 AS label FROM BS_AREA WHERE AREA_SECT1 = @AREA_SECT1";

            using(SqlConnection conn = GetConnection())
            {
                List<Option> result = new List<Option>();
                try
                {
                    result = conn.Query<Option>(sqlQry,new { AREA_SECT1= AREA_SECT1 }).ToList();
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得街道路段列表
        /// </summary>
        /// <param name="AREA_SECT1"></param>
        /// <param name="AREA_SECT2"></param>
        /// <returns></returns>
        public List<Option> GetSect3Option(string AREA_SECT1, string AREA_SECT2)
        {
            string sqlQry = @"SELECT DISTINCT AREA_SECT3 AS value, AREA_SECT3 AS label
                              FROM BS_AREA
                              WHERE AREA_SECT1=@AREA_SECT1 AND AREA_SECT2=@AREA_SECT2";
            using (SqlConnection conn = GetConnection())
            {
                List<Option> result = new List<Option>();
                try
                {
                    result = conn.Query<Option>(sqlQry, new { AREA_SECT1 = AREA_SECT1, AREA_SECT2 = AREA_SECT2 }).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得站所列表
        /// </summary>
        /// <returns></returns>
        public List<Option> GetDeptOption()
        {
            string sqlQry = @"SELECT DEPT_ID AS value,DEPT_NAME AS label
                              FROM BS_DEPT";

            using(SqlConnection conn = GetConnection())
            {
                List<Option> result = new List<Option>();
                try
                {
                    result = conn.Query<Option>(sqlQry).ToList();
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <returns></returns>
        public List<Form> GetTableData(Form data)
        {
            string sqlQry = @"SELECT AREA_ID,AREA_SECT1,AREA_SECT2,AREA_SECT3,AREA_SECT4
                                ,ZIP_CODE,REMOTE_FG,TRSF_FG,DEPT_ID,DEPT_NAME,SD_CODE
                                ,MD_CODE,STACK_AREA
                              FROM BS_AREA
                              WHERE AREA_ID LIKE @AREA_ID AND DEPT_ID LIKE @DEPT_ID
                                AND AREA_SECT1 LIKE @AREA_SECT1 AND AREA_SECT2 LIKE @AREA_SECT2";

            using(SqlConnection conn = GetConnection())
            {
                List<Form> result = new List<Form>();
                try
                {
                    data.AREA_ID = "%" + data.AREA_ID + "%";
                    data.DEPT_ID = "%" + data.DEPT_ID + "%";
                    data.AREA_SECT1 = "%" + data.AREA_SECT1 + "%";
                    data.AREA_SECT2 = "%" + data.AREA_SECT2 + "%";
                    result = conn.Query<Form>(sqlQry, data).ToList();
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 新增配送區路段資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Form data)
        {
            string sqlIn = @"INSERT INTO BS_AREA
                                (AREA_SECT1,AREA_SECT2,AREA_SECT3,AREA_SECT4
                                ,DEPT_ID,DEPT_NAME,ZIP_CODE,SD_CODE,MD_CODE,STACK_AREA
                                ,REMOTE_FG,TRSF_FG,CREATE_USER,CREATE_TIME)
                             SELECT @AREA_SECT1,@AREA_SECT2,@AREA_SECT3,@AREA_SECT4
                                ,DEPT_ID,DEPT_NAME,@ZIP_CODE,@SD_CODE,@MD_CODE,@STACK_AREA
                                ,@REMOTE_FG,@TRSF_FG,@CREATE_USER,GETDATE()
                             FROM BS_DEPT
                             WHERE DEPT_ID = @DEPT_ID";

            using (SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    data.CREATE_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, data);
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 編輯配送區路段資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Edit(Form data)
        {
            string sqlUp = @"UPDATE BS_AREA SET 
                                AREA_SECT1 = @AREA_SECT1,
                                AREA_SECT2 = @AREA_SECT2,
                                AREA_SECT3 = @AREA_SECT3,
                                AREA_SECT4 = @AREA_SECT4,
                                ZIP_CODE = @ZIP_CODE,
                                REMOTE_FG = @REMOTE_FG,
                                TRSF_FG = @TRSF_FG,
                                DEPT_ID = @DEPT_ID,
                                DEPT_NAME = (SELECT DEPT_NAME FROM BS_DEPT WHERE DEPT_ID = @DEPT_ID),
                                SD_CODE = @SD_CODE,
                                MD_CODE = @MD_CODE,
                                STACK_AREA = @STACK_AREA,
                                EDIT_USER = @EDIT_USER,
                                EDIT_TIME = GETDATE()
                             WHERE AREA_ID = @AREA_ID";
            
            using(SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    data.EDIT_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlUp, data);
                }catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 批次修改
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int BatchEdit(BatchForm data)
        {
            string sqlUp = @"UPDATE BS_AREA SET
                                DEPT_ID = @DEPT_ID,
                                DEPT_NAME = (SELECT DEPT_NAME FROM BS_DEPT WHERE DEPT_ID = @DEPT_ID),
                                SD_CODE = @SD_CODE,
                                MD_CODE = @MD_CODE,
                                STACK_AREA = @STACK_AREA,
                                EDIT_USER = @EDIT_USER,
                                EDIT_TIME = GETDATE()
                             WHERE AREA_ID = @AREA_ID";
            using (SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    List<object> param = new List<object>();
                    foreach (string AREA_ID in data.AREA_ID)
                    {
                        param.Add(new
                        {
                            AREA_ID = AREA_ID,
                            DEPT_ID = data.DEPT_ID,
                            SD_CODE = data.SD_CODE,
                            MD_CODE = data.MD_CODE,
                            STACK_AREA = data.STACK_AREA,
                            EDIT_USER = SessionUtil.Current.UserId
                        });
                    }
                    result = conn.Execute(sqlUp, param);
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 批次刪除路段資料
        /// </summary>
        /// <param name="AREA_ID"></param>
        /// <returns></returns>
        public int Del(List<string> AREA_ID)
        {
            string sqlDel = @"DELETE FROM BS_AREA WHERE AREA_ID = @AREA_ID";

            using(SqlConnection conn = GetConnection())
            {
                int result;
                try
                {
                    List<object> param = new List<object>();
                    foreach(string id in AREA_ID)
                    {
                        param.Add(new { AREA_ID = id });
                    }
                    result = conn.Execute(sqlDel, param);
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 匯入資料
        /// </summary>
        /// <returns>200:成功;other:失敗</returns>
        public string UploadFile(HttpPostedFileBase file)
        {
            string sqlStr = @"IF EXISTS (SELECT * FROM BS_AREA 
                                    WHERE ZIP_CODE = @ZIP_CODE 
                                        AND AREA_SECT1 = @AREA_SECT1
                                        AND AREA_SECT2 = @AREA_SECT2 
                                        AND AREA_SECT3 = @AREA_SECT3
                                        AND AREA_SECT4 = @AREA_SECT4) 
                                BEGIN
                                    UPDATE BS_AREA SET
                                        DEPT_ID = @DEPT_ID,
                                        DEPT_NAME = @DEPT_NAME,
                                        MD_CODE = @MD_CODE,
                                        SD_CODE = @SD_CODE,
                                        STACK_AREA = @STACK_AREA,
                                        EDIT_USER = @EDIT_USER,
                                        EDIT_TIME = GETDATE()
                                    WHERE ZIP_CODE = @ZIP_CODE
                                        AND AREA_SECT1 = @AREA_SECT1
                                        AND AREA_SECT2 = @AREA_SECT2 
                                        AND AREA_SECT3 = @AREA_SECT3
                                        AND AREA_SECT4 = @AREA_SECT4
                                END
                                ELSE
                                BEGIN
                                    INSERT INTO BS_AREA 
                                        (ZIP_CODE,AREA_SECT1,AREA_SECT2,AREA_SECT3
                                        ,AREA_SECT4,DEPT_ID,DEPT_NAME,MD_CODE,SD_CODE
                                        ,STACK_AREA,CREATE_USER,CREATE_TIME)
                                    VALUES
                                        (@ZIP_CODE,@AREA_SECT1,@AREA_SECT2,@AREA_SECT3
                                        ,@AREA_SECT4,@DEPT_ID,@DEPT_NAME,@MD_CODE,@SD_CODE
                                        ,@STACK_AREA,@CREATE_USER,GETDATE())
                                END";
            using(SqlConnection conn = GetConnection())
            {
                string result;
                int count = 0;//計算寫了幾筆
                try
                {
                    XSSFWorkbook workbook = new XSSFWorkbook(file.InputStream); ;
                    XSSFSheet sheet = (XSSFSheet)workbook.GetSheetAt(0);
                    for (int i = 1; i <= sheet.LastRowNum; i++)
                    {
                        XSSFRow row = (XSSFRow)sheet.GetRow(i);
                        DynamicParameters param = new DynamicParameters();

                        for (int j = 2; j <= row.LastCellNum; j++)
                        {
                            ICell cell = row.GetCell(j);
                            param.Add("@EDIT_USER", SessionUtil.Current.UserId);
                            param.Add("@CREATE_USER", SessionUtil.Current.UserId);
                            switch (j)
                            {
                                case 2:
                                    param.Add("@ZIP_CODE", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 3:
                                    param.Add("@AREA_SECT1", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 4:
                                    param.Add("@AREA_SECT2", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 5:
                                    param.Add("@AREA_SECT3", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 6:
                                    param.Add("@AREA_SECT4", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 7:
                                    param.Add("@DEPT_ID", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 8:
                                    param.Add("@DEPT_NAME", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 9:
                                    param.Add("@MD_CODE", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 10:
                                    param.Add("@SD_CODE", (cell == null) ? "" : cell.ToString());
                                    break;
                                case 11:
                                    param.Add("@STACK_AREA", (cell == null) ? "" : cell.ToString());
                                    break;
                            }
                        }
                        conn.Execute(sqlStr, param);
                        count++;
                    }
                    result = "200|"+count;
                }
                catch(Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = e.Message;
                }
                return result;
            }
        }
    }
}