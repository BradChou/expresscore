﻿using Dapper;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.LoginModel;

namespace FSE_WEB.Dao
{
    public class LoginDao :BasicDao
    {
        /// <summary>
        /// 判斷帳號密碼
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PASSWORD"></param>
        /// <returns></returns>
        public UserInfo Login(string USER_ID, string USER_PWD)
        {
            string sqlQry = @"SELECT SU.USER_ID,USER_NAME,ROLE_ID
                              FROM SYS_USER SU
                              JOIN SYS_USER_ROLE SUR ON SU.USER_ID = SUR.USER_ID
                              WHERE SU.USER_ID = @USER_ID AND USER_PWD = @USER_PWD";

            SqlConnection conn = GetConnection();
            UserInfo result = new UserInfo();
            try
            {
                USER_PWD = EncryUtil.Md5(USER_PWD);
                result = conn.QuerySingleOrDefault<UserInfo>(sqlQry,new { USER_ID=USER_ID, USER_PWD = USER_PWD });
                //如果找不到資料會使result變成null
                if(result != null)
                {
                    SessionUtil.Current.UserId = result.USER_ID;
                    SessionUtil.Current.UserName = result.USER_NAME;
                    SessionUtil.Current.RoleId = result.ROLE_ID;
                    SessionUtil.Current.LoginTime = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                    result.token = TokenUtil.CreateToken();
                }
                else //初始化result
                {
                    result = new UserInfo();
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result.USER_ID = "400";
            }
            conn.Close();
            return result;
        }
    }
}