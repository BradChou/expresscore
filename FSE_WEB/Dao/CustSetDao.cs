﻿using Dapper;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.BasicModel;
using static FSE_WEB.Models.CustSetModel;

namespace FSE_WEB.Dao
{
    public class CustSetDao : BasicDao
    {
        /// <summary>
        /// 取得下拉選單資料
        /// </summary>
        /// <param name="type"></param>
        /// <param name="condition">地址:代表City|銀行:代表Bank</param>
        /// <returns></returns>
        public List<Option> GetOption(string type,string condition)
        {
            string sqlQry = "";
            //縣市
            string sqlQry_city = @"SELECT DISTINCT AREA_SECT1 AS value,AREA_SECT1 AS label FROM BS_AREA";
            //鄉鎮區(市)
            string sqlQry_area = @"SELECT DISTINCT AREA_SECT2 AS value,AREA_SECT2 AS label FROM BS_AREA WHERE AREA_SECT1 = @condition";
            //銀行
            string sqlQry_bank = @"SELECT BANK_ID as value,CONVERT(varchar,BANK_ID)+'-'+BANK_NAME as label FROM BS_BANK WHERE LEN(BANK_ID)=3";
            //分行
            string sqlQry_branch = @"SELECT REPLACE(BANK_BRANCH,' ','') as value,REPLACE(BANK_BRANCH,' ','') as label FROM BS_BANK WHERE BANK_BRANCH IS NOT NULL AND LEFT(BANK_ID,3)=@condition";
            //開發站所
            string sqlQry_dept = @"SELECT DEPT_ID as value,DEPT_NAME as label FROM BS_DEPT";
            //可用產品
            string sqlQry_prod = @"SELECT PROD_ID as value,PROD_NAME as label FROM BS_PROD 
                        WHERE ACT_FG='Y' AND PROD_ID NOT IN 
                        (SELECT PROD_ID FROM DT_CUST_PROD WHERE CUST_CODE=@condition)";
            List<Option> result = new List<Option>();

            try
            {
                switch (type)
                {
                    case "city":
                        sqlQry = sqlQry_city;
                        break;
                    case "area":
                        sqlQry = sqlQry_area;
                        break;
                    case "bank":
                        sqlQry = sqlQry_bank;
                        break;
                    case "branch":
                        sqlQry = sqlQry_branch;
                        break;
                    case "dept":
                        sqlQry = sqlQry_dept;
                        break;
                    case "prod":
                        sqlQry = sqlQry_prod;
                        break;
                }
                using (SqlConnection conn = GetConnection())
                {
                    result = conn.Query<Option>(sqlQry, new { condition = condition }).ToList();
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }
        
        /// <summary>
        /// 取得表格資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<Form> GetTableData(Form form)
        {
            string sqlQry = @"SELECT * FROM DT_CUST";

            List<Form> result = new List<Form>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    string sqlCondition = " WHERE 1=1";
                    if (form.CUST_CODE != null) sqlCondition += " AND CUST_CODE LIKE '%'+@CUST_CODE+'%'";
                    if (form.CHI_NAME != null) sqlCondition += " AND CHI_NAME LIKE '%'+@CHI_NAME+'%'";
                    if (form.CUST_ID != null) sqlCondition += " AND CUST_ID LIKE '%'+@CUST_ID+'%'";
                    if (form.CUST_GROUP != null) sqlCondition += " AND CUST_GROUP LIKE '%'+@CUST_GROUP+'%'";
                    result = conn.Query<Form>(sqlQry + sqlCondition, form).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得該客戶可用產品資料
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <returns></returns>
        public List<FormProd> GetProdTableData(string CUST_CODE)
        {
            string sqlQry = @"SELECT A.*,B.PROD_NAME FROM DT_CUST_PROD A
                            JOIN BS_PROD B ON A.PROD_ID=B.PROD_ID
                            WHERE CUST_CODE=@CUST_CODE";

            List<FormProd> result = new List<FormProd>();

            using (SqlConnection conn = GetConnection())
            {

                try
                {
                    result = conn.Query<FormProd>(sqlQry, new { CUST_CODE = CUST_CODE }).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 取得該客戶使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<FormUser> GetUserTableData(FormUser form)
        {
            string sqlQry = @"SELECT * FROM DT_CUST_USER";

            List<FormUser> result = new List<FormUser>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    string sqlCondition = " WHERE CUST_CODE=@CUST_CODE";
                    if (form.USER_CODE != null) sqlCondition += " AND USER_CODE LIKE '%'+@USER_CODE+'%'";
                    if (form.USER_NAME != null) sqlCondition += " AND USER_NAME LIKE '%'+@USER_NAME+'%'";
                    if (form.ACT_FG != null && form.ACT_FG != "%") sqlCondition += " AND ACT_FG = @ACT_FG";

                    result = conn.Query<FormUser>(sqlQry + sqlCondition, form).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }
        
        /// <summary>
        /// 取得該客戶常用收送貨地址資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<FormAddr> GetAddrTableData(FormAddr form)
        {
            string sqlQry = @"SELECT A.*,B.DEPT_NAME FROM DT_CUST_ADDR A
                    LEFT JOIN BS_DEPT B ON A.CLT_DEPT=B.DEPT_ID";

            List<FormAddr> result = new List<FormAddr>();
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    string sqlCondition = " WHERE CUST_CODE=@CUST_CODE";
                    if (form.PS_COMP != null) sqlCondition += " AND PS_COMP LIKE '%'+@PS_COMP+'%'";
                    if (form.CONTACT != null) sqlCondition += " AND A.CONTACT LIKE '%'+@CONTACT+'%'";
                    if (form.PICKSEND_FG != null && form.PICKSEND_FG != "%") sqlCondition += string.Format(" AND {0}_FG='Y'", form.PICKSEND_FG);

                    result = conn.Query<FormAddr>(sqlQry + sqlCondition, form).ToList();
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 新增客戶資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            string sqlCheck = @"SELECT COUNT(*) AS CNT FROM DT_CUST WHERE CUST_CODE=@CUST_CODE";
            string sqlIn = @"INSERT INTO DT_CUST (
                        CUST_CODE,CUST_ID,CHI_NAME,BRF_NAME,CHI_CITY
                        ,CHI_AREA,CHI_ADDR,INV_CITY,INV_AREA,INV_ADDR
                        ,CONTACT,TEL_NO1,TEL_NO2,FAX_NO,CUST_GROUP
                        ,EMAIL_LIST,BANK_ID,BANK_NAME,BANK_BRANCH,BANK_ACCT
                        ,BANK_CUST,DVLP_DEPT,ACT_FG,REMARKS
                        ,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME
                    )
                    VALUES(
                        @CUST_CODE,@CUST_ID,@CHI_NAME,@BRF_NAME,@CHI_CITY
                        ,@CHI_AREA,@CHI_ADDR,@INV_CITY,@INV_AREA,@INV_ADDR
                        ,@CONTACT,@TEL_NO1,@TEL_NO2,@FAX_NO,@CUST_GROUP
                        ,@EMAIL_LIST,@BANK_ID,(SELECT BANK_NAME FROM BS_BANK WHERE BANK_ID=@BANK_ID),@BANK_BRANCH,@BANK_ACCT
                        ,@BANK_CUST,@DVLP_DEPT,@ACT_FG,@REMARKS
                        ,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE()
                    )";


            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.CREATE_USER = SessionUtil.Current.UserId;
                    var check = conn.QuerySingleOrDefault(sqlCheck, form);
                    if (check.CNT > 0) return -2;

                    result = conn.Execute(sqlIn, form);

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 編輯客戶資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            string sqlIn = @"UPDATE DT_CUST SET 
                    CUST_ID=@CUST_ID
                    ,CHI_NAME=@CHI_NAME
                    ,BRF_NAME=@BRF_NAME
                    ,CHI_CITY=@CHI_CITY
                    ,CHI_AREA=@CHI_AREA
                    ,CHI_ADDR=@CHI_ADDR
                    ,INV_CITY=@INV_CITY
                    ,INV_AREA=@INV_AREA
                    ,INV_ADDR=@INV_ADDR
                    ,CONTACT=@CONTACT
                    ,TEL_NO1=@TEL_NO1
                    ,TEL_NO2=@TEL_NO2
                    ,FAX_NO=@FAX_NO
                    ,CUST_GROUP=@CUST_GROUP
                    ,EMAIL_LIST=@EMAIL_LIST
                    ,BANK_ID=@BANK_ID
                    ,BANK_NAME=(SELECT BANK_NAME FROM BS_BANK WHERE BANK_ID=@BANK_ID)
                    ,BANK_BRANCH=@BANK_BRANCH
                    ,BANK_ACCT=@BANK_ACCT
                    ,BANK_CUST=@BANK_CUST
                    ,DVLP_DEPT=@DVLP_DEPT
                    ,ACT_FG=@ACT_FG
                    ,REMARKS=@REMARKS
                    ,EDIT_USER=@EDIT_USER
                    ,EDIT_TIME=GETDATE()
                    WHERE CUST_CODE=@CUST_CODE";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.EDIT_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, form);

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 刪除客戶資料
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <returns></returns>
        public int Delete(string CUST_CODE)
        {
            string sqlDel = @"DELETE FROM DT_CUST{0} WHERE CUST_CODE=@CUST_CODE";
            int result;
            using (SqlConnection conn = GetConnection())
            {
                using (SqlTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        var param = new { CUST_CODE = CUST_CODE };
                        //刪除客戶資料
                        conn.Execute(string.Format(sqlDel, ""), param, transaction: tran);
                        //刪除該客戶可用產品資料
                        conn.Execute(string.Format(sqlDel, "_PROD"), param, transaction: tran);
                        //刪除該客戶使用者資料
                        conn.Execute(string.Format(sqlDel, "_USER"), param, transaction: tran);
                        //刪除該客戶常用地址資料
                        conn.Execute(string.Format(sqlDel, "_ADDR"), param, transaction: tran);
                        tran.Commit();
                        result = 1;
                    }
                    catch (Exception e)
                    {
                        tran.Rollback();
                        LogUtil.ErrorLog(e.ToString());
                        result = -1;
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// 新增該客戶可用產品
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddProd(FormProd form)
        {
            string sqlIn = @"INSERT INTO DT_CUST_PROD (CUST_CODE,PROD_ID,EDIT_USER,EDIT_TIME)
                    VALUES(@CUST_CODE,@PROD_ID,@EDIT_USER,GETDATE())";
            
            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.EDIT_USER = SessionUtil.Current.UserId;
                    result = conn.Execute(sqlIn, form);

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 刪除該客戶可用產品
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <param name="PROD_ID"></param>
        /// <returns></returns>
        public int DeleteProd(string CUST_CODE,string PROD_ID)
        {
            string sqlIn = @"DELETE FROM DT_CUST_PROD WHERE CUST_CODE=@CUST_CODE AND PROD_ID=@PROD_ID";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Execute(sqlIn, new { CUST_CODE = CUST_CODE, PROD_ID = PROD_ID });

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 新增該客戶使用者
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddUser(FormUser form)
        {
            string sqlCheck = @"SELECT COUNT(*) AS CNT FROM DT_CUST_USER WHERE USER_CODE=@USER_CODE";
            string sqlIn = @"INSERT INTO DT_CUST_USER (
                        CUST_CODE,USER_CODE,USER_NAME,USER_PWD,EMAIL
                        ,TEL_NO,CELL_PHONE,ACT_FG,REMARKS
                        ,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME
                        )
                    VALUES(
                        @CUST_CODE,@USER_CODE,@USER_NAME,@USER_PWD,@EMAIL
                        ,@TEL_NO,@CELL_PHONE,@ACT_FG,@REMARKS
                        ,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE()
                        )";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    var check = conn.QuerySingleOrDefault(sqlCheck, form);
                    if (check.CNT > 0) return -2;

                    form.CREATE_USER = SessionUtil.Current.UserId;
                    form.USER_PWD = EncryUtil.Md5(form.USER_PWD);
                    result = conn.Execute(sqlIn, form);

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 編輯該客戶指定使用者
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int EditUser(FormUser form)
        {
            string sqlIn = @"UPDATE  DT_CUST_USER SET
                            USER_NAME=@USER_NAME
                            ,USER_PWD=@USER_PWD
                            ,EMAIL=@EMAIL
                            ,TEL_NO=@TEL_NO
                            ,CELL_PHONE=@CELL_PHONE
                            ,ACT_FG=@ACT_FG
                            ,REMARKS=@REMARKS
                            ,EDIT_USER=@EDIT_USER
                            ,EDIT_TIME=GETDATE()
                        WHERE CUST_CODE=@CUST_CODE AND USER_CODE=@USER_CODE";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.EDIT_USER = SessionUtil.Current.UserId;
                    form.USER_PWD = EncryUtil.Md5(form.USER_PWD);
                    result = conn.Execute(sqlIn, form);

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 刪除該客戶指定使用者
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <param name="USER_CODE"></param>
        /// <returns></returns>
        public int DeleteUser(string CUST_CODE, string USER_CODE)
        {
            string sqlIn = @"DELETE FROM DT_CUST_USER WHERE CUST_CODE=@CUST_CODE AND USER_CODE=@USER_CODE";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Execute(sqlIn, new { CUST_CODE = CUST_CODE, USER_CODE = USER_CODE });

                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 重置密碼
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int ResetPwd(FormUser form)
        {
            //先取得密碼長度限制
            string sqlLimit = @"SELECT (SELECT SET_VALUE FROM SYS_SET WHERE SET_ID='pwdMinLength') AS 'MIN',
                    (SELECT SET_VALUE FROM SYS_SET WHERE SET_ID='pwdMaxLength') AS 'MAX'";
            //更新資料庫
            string sqlUp = @"UPDATE DT_CUST_USER SET USER_PWD=@USER_PWD WHERE CUST_CODE=@CUST_CODE AND USER_CODE=@USER_CODE";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    try
                    {
                        var limit = conn.QuerySingle(sqlLimit, transaction: trans);
                        //產生密碼
                        string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
                        Random rd = new Random();
                        int passwordLength = rd.Next(int.Parse(limit.MIN), int.Parse(limit.MAX));
                        char[] chars = new char[passwordLength];
                        for (int i = 0; i < passwordLength; i++)
                        {
                            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                        }
                        string nPwd = new string(chars);
                        //更新資料庫
                        form.USER_PWD = EncryUtil.Md5(nPwd);
                        result = conn.Execute(sqlUp, form, transaction: trans);
                        //寄信
                        List<string> to = new List<string>();
                        to.Add(form.EMAIL);
                        List<string> cc = new List<string>();
                        cc.Add("");
                        string subject = "[FSE 雲端物流系統]密碼變更通知";
                        string bodyMessage = "您的新密碼如下：<br /><b>" + nPwd + "</b>";
                        MailUtil.SendMail(to, cc, subject, bodyMessage, mailformat: "html");

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        LogUtil.ErrorLog(e.ToString());
                        result = -1;
                    }
                    return result;
                }
            }
        }

        /// <summary>
        /// 新增常用地址
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddAddr(FormAddr form)
        {
            string sqlIn = @"INSERT INTO DT_CUST_ADDR(
                        CUST_CODE,PICK_FG,SEND_FG,PS_CITY,PS_AREA
                        ,PS_ADDR,PS_COMP,CONTACT,TEL_NO1,TEL_NO2
                        ,CLT_DEPT,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME)
                    VALUES(
                        @CUST_CODE,@PICK_FG,@SEND_FG,@PS_CITY,@PS_AREA
                        ,@PS_ADDR,@PS_COMP,@CONTACT,@TEL_NO1,@TEL_NO2
                        ,@CLT_DEPT,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE()
                    )";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.CREATE_USER = SessionUtil.Current.UserId;
                    form.PICK_FG = form.PICK_FG == "True" ? "Y" : "";
                    form.SEND_FG = form.SEND_FG == "True" ? "Y" : "";
                    result = conn.Execute(sqlIn, form);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 編輯常用地址
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int EditAddr(FormAddr form)
        {
            string sqlUp = @"UPDATE DT_CUST_ADDR SET
                    PICK_FG=@PICK_FG,SEND_FG=@SEND_FG,PS_CITY=@PS_CITY,PS_AREA=@PS_AREA,PS_ADDR=@PS_ADDR
                    ,PS_COMP=@PS_COMP,CONTACT=@CONTACT,TEL_NO1=@TEL_NO1,TEL_NO2=@TEL_NO2,CLT_DEPT=@CLT_DEPT
                    ,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE() WHERE PS_SEQ=@PS_SEQ";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    form.EDIT_USER = SessionUtil.Current.UserId;
                    form.PICK_FG = form.PICK_FG == "True" ? "Y" : "";
                    form.SEND_FG = form.SEND_FG == "True" ? "Y" : "";
                    result = conn.Execute(sqlUp, form);
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }

        /// <summary>
        /// 刪除常用地址
        /// </summary>
        /// <param name="PS_SEQ"></param>
        /// <returns></returns>
        public int DeleteAddr(string PS_SEQ)
        {
            string sqlDel = @"DELETE FROM DT_CUST_ADDR WHERE PS_SEQ=@PS_SEQ";

            int result;
            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    result = conn.Execute(sqlDel, new { PS_SEQ = PS_SEQ });
                }
                catch (Exception e)
                {
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
                return result;
            }
        }
    }
}