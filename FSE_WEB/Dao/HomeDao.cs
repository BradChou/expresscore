﻿using Dapper;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.HomeModel;

namespace FSE_WEB.Dao
{
    public class HomeDao:BasicDao
    {
        
        /// <summary>
        /// 取得使用者個人功能選單
        /// </summary>
        /// <param name="root">想要展開的根</param>
        /// <param name="type">想要展開的功能類別</param>
        /// <returns></returns>
        public List<Menu> GetMenu(string root,string type)
        {
            string sqlQry = @"EXEC SHOW_USER_FUNC_MENU @USER_ID,@ROOT,@FUNC_TYPE";

            SqlConnection conn = GetConnection();
            List<Menu> result = new List<Menu>();

            try
            {
                object param = new
                {
                    USER_ID = SessionUtil.Current.UserId,
                    ROOT = root,
                    FUNC_TYPE = type
                };
                result = conn.Query<Menu>(sqlQry, param).ToList();
            }catch(Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            conn.Close();
            return result;
        }
    }
}