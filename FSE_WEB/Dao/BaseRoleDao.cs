﻿using Dapper;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static FSE_WEB.Models.BaseRoleModel;

namespace FSE_WEB.Dao
{
    public class BaseRoleDao : BasicDao
    {
        public List<Table> GetTableData(string ROLE_ID, string ROLE_NAME,string ACT_FG)
        {
            string sqlQry = @"SELECT * FROM BS_ROLE
                    WHERE ROLE_ID LIKE @ROLE_ID AND ROLE_NAME LIKE @ROLE_NAME 
                    AND ACT_FG LIKE @ACT_FG";
            SqlConnection conn = GetConnection();
            List<Table> result = new List<Table>();
            try
            {
                object param = new
                {
                    ROLE_ID = "%" + ROLE_ID + "%",
                    ROLE_NAME = "%" + ROLE_NAME + "%",
                    ACT_FG = "%" + ACT_FG + "%"
                };
                result = conn.Query<Table>(sqlQry, param).ToList();
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }

        public List<SettingTable> GetSettingTableData(string ROLE_ID)
        {
            SqlConnection conn = GetConnection();
            List<SettingTable> result = new List<SettingTable>();
            try
            {
                object param = new
                {
                    RootID = "GOD",
                    Ftype = "WEB",
                    RoleID = ROLE_ID
                };
                List<SettingTable> sqlData = conn.Query<SettingTable>("GET_ROLE_FUNC_MENU", param, commandType: CommandType.StoredProcedure).ToList();
                for (int i = 0; i < sqlData.Count; i++)
                {
                    if (sqlData[i].subNum > 0) sqlData[i]._children = new List<SettingTable>();

                    int index = result.FindIndex(x => x.FUNC_ID.Equals(sqlData[i].PARENT));
                    int indexChi = result.FindIndex(x => x.FUNC_ID.Equals(sqlData[i].PARENT.Substring(0, 1) + "000"));
                    //第三層目前還沒解決
                    if (index > -1) //代表父項已在list內
                    {
                        result[index]._children.Add(sqlData[i]);
                    }
                    else if (indexChi > -1) //代表是同個項目，但父項目是在此父項目底下
                    {
                        index = result[indexChi]._children.FindIndex(x => x.FUNC_ID.Equals(sqlData[i].PARENT));
                        result[indexChi]._children[index]._children.Add(sqlData[i]);
                    }
                    else
                    {
                        result.Add(sqlData[i]); //父項直接塞入
                    }
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
            }
            return result;
        }
        
        /// <summary>
        /// 新增角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns>-1:error|-2:重複名稱</returns>
        public int Add(Form form)
        {
            string sqlCnt = @"SELECT COUNT(*) AS CNT FROM BS_ROLE WHERE ROLE_NAME=@ROLE_NAME";
            string sqlIn = @"INSERT INTO BS_ROLE
                    VALUES(@ROLE_ID,@ROLE_NAME,@DSP_ORD,@REMARKS,@ACT_FG,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    ROLE_ID = form.ROLE_ID,
                    ROLE_NAME = form.ROLE_NAME,
                    DSP_ORD = form.DSP_ORD,
                    REMARKS = form.REMARKS,
                    ACT_FG = form.ACT_FG,
                    CREATE_USER = SessionUtil.Current.UserId
                };
                if (conn.QuerySingle(sqlCnt, param).CNT > 0) {
                    result = -2;
                }
                else
                {
                    result = conn.Execute(sqlIn, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            return result;
        }

        /// <summary>
        /// 編輯角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns>-1:error|-2:重複名稱</returns>
        public int Edit(Form form)
        {
            string sqlCnt = @"SELECT COUNT(*) AS CNT FROM BS_ROLE WHERE ROLE_NAME=@ROLE_NAME AND ROLE_ID<>@ROLE_ID";
            string sqlUp = @"UPDATE BS_ROLE SET
                    ROLE_NAME=@ROLE_NAME,DSP_ORD=@DSP_ORD,REMARKS=@REMARKS,ACT_FG=@ACT_FG
                    ,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE()
                    WHERE ROLE_ID=@ROLE_ID";
            SqlConnection conn = GetConnection();
            int result;
            try
            {
                object param = new
                {
                    ROLE_ID = form.ROLE_ID,
                    ROLE_NAME = form.ROLE_NAME,
                    DSP_ORD = form.DSP_ORD,
                    REMARKS = form.REMARKS,
                    ACT_FG = form.ACT_FG,
                    EDIT_USER = SessionUtil.Current.UserId
                };
                if (conn.QuerySingle(sqlCnt, param).CNT > 0)
                {
                    result = -2;
                }
                else
                {
                    result = conn.Execute(sqlUp, param);
                }
            }
            catch (Exception e)
            {
                LogUtil.ErrorLog(e.ToString());
                result = -1;
            }
            return result;
        }

        /// <summary>
        /// 儲存權限資料
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int SaveSetting(string ROLE_ID,List<SettingTable> data)
        {
            string sqlCount = @"SELECT COUNT(*) AS CNT FROM SYS_ROLE_FUNC WHERE FUNC_ID=@FUNC_ID AND ROLE_ID=@ROLE_ID";
            string sqlUp = @"UPDATE SYS_ROLE_FUNC SET
                    CAN_RUN=@CAN_RUN,CAN_SEARCH=@CAN_SEARCH,CAN_ADD=@CAN_ADD,CAN_MODIFY=@CAN_MODIFY,CAN_DEL=@CAN_DEL
                    ,CAN_PRINT=@CAN_PRINT,EDIT_USER=@EDIT_USER,EDIT_TIME=GETDATE() WHERE FUNC_ID=@FUNC_ID AND ROLE_ID=@ROLE_ID";
            string sqlIn = @"INSERT INTO SYS_ROLE_FUNC
                    VALUES(@FUNC_ID,@ROLE_ID,@CAN_RUN,@CAN_SEARCH,@CAN_ADD
                    ,@CAN_MODIFY,@CAN_DEL,@CAN_PRINT,@EDIT_USER,GETDATE(),@EDIT_USER,GETDATE())";
            SqlConnection conn = GetConnection();
            int result;
            using (var tran = conn.BeginTransaction())
            {
                try
                {
                    foreach (SettingTable item in data)
                    {//最高項目不用存
                        foreach (SettingTable item1 in item._children)
                        {
                            object param = new
                            {
                                FUNC_ID = item1.FUNC_ID,
                                ROLE_ID = ROLE_ID,
                                CAN_RUN = item1.CAN_RUN.Equals("True") ? "Y" : "N",
                                CAN_SEARCH = item1.CAN_SEARCH.Equals("True") ? "Y" : "N",
                                CAN_ADD = item1.CAN_ADD.Equals("True") ? "Y" : "N",
                                CAN_MODIFY = item1.CAN_MODIFY.Equals("True") ? "Y" : "N",
                                CAN_DEL = item1.CAN_DEL.Equals("True") ? "Y" : "N",
                                CAN_PRINT = item1.CAN_PRINT.Equals("True") ? "Y" : "N",
                                EDIT_USER = SessionUtil.Current.UserId
                            };
                            if (conn.QuerySingle(sqlCount, param, transaction: tran).CNT > 0)
                            {//Update
                                conn.Execute(sqlUp, param, transaction: tran);
                            }
                            else
                            {//Insert
                                conn.Execute(sqlIn, param, transaction: tran);
                            }
                            if (item.subNum == 0) continue;
                            foreach (SettingTable item2 in item1._children)
                            {
                                param = new
                                {
                                    FUNC_ID = item2.FUNC_ID,
                                    ROLE_ID = ROLE_ID,
                                    CAN_RUN = item2.CAN_RUN.Equals("True") ? "Y" : "N",
                                    CAN_SEARCH = item2.CAN_SEARCH.Equals("True") ? "Y" : "N",
                                    CAN_ADD = item2.CAN_ADD.Equals("True") ? "Y" : "N",
                                    CAN_MODIFY = item2.CAN_MODIFY.Equals("True") ? "Y" : "N",
                                    CAN_DEL = item2.CAN_DEL.Equals("True") ? "Y" : "N",
                                    CAN_PRINT = item2.CAN_PRINT.Equals("True") ? "Y" : "N",
                                    EDIT_USER = SessionUtil.Current.UserId
                                };
                                if (conn.QuerySingle(sqlCount, param, transaction: tran).CNT > 0)
                                {//Update
                                    conn.Execute(sqlUp, param, transaction: tran);
                                }
                                else
                                {//Insert
                                    conn.Execute(sqlIn, param, transaction: tran);
                                }
                            }
                        }
                    }
                    tran.Commit();
                    result = 1;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
            }
            conn.Close();
            return result;

        }

        /// <summary>
        /// 新增角色並複製指定權限給新角色
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Copy(string ROLE_ID,Form form)
        {
            string sqlCnt = @"SELECT COUNT(*) AS CNT FROM BS_ROLE WHERE ROLE_NAME=@ROLE_NAME";
            string sqlIn = @"INSERT INTO BS_ROLE
                    VALUES(@ROLE_ID,@ROLE_NAME,@DSP_ORD,@REMARKS,@ACT_FG,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE())";
            string sqlCopy = @"INSERT INTO SYS_ROLE_FUNC
                    SELECT FUNC_ID,@NEW_ROLE_ID,CAN_RUN,CAN_SEARCH,CAN_ADD,CAN_MODIFY
                    ,CAN_DEL,CAN_PRINT,@CREATE_USER,GETDATE(),@CREATE_USER,GETDATE()
                    FROM SYS_ROLE_FUNC WHERE ROLE_ID=@ROLE_ID";
            SqlConnection conn = GetConnection();
            int result;
            using (var tran = conn.BeginTransaction())
            {
                try
                {
                    object param = new
                    {
                        ROLE_ID = form.ROLE_ID,
                        ROLE_NAME = form.ROLE_NAME,
                        DSP_ORD = form.DSP_ORD,
                        REMARKS = form.REMARKS,
                        ACT_FG = form.ACT_FG,
                        CREATE_USER = SessionUtil.Current.UserId
                    };
                    if (conn.QuerySingle(sqlCnt, param, transaction: tran).CNT > 0)
                    {
                        result = -2;
                    }
                    else
                    {
                        conn.Execute(sqlIn, param, transaction: tran);
                        param = new
                        {
                            ROLE_ID = ROLE_ID,
                            NEW_ROLE_ID = form.ROLE_ID,
                            CREATE_USER = SessionUtil.Current.UserId
                        };
                        result = conn.Execute(sqlCopy, param, transaction: tran);
                    }
                    tran.Commit();
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    LogUtil.ErrorLog(e.ToString());
                    result = -1;
                }
            }
            conn.Close();
            return result;
        }
    }
}