﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseRoleModel;

namespace FSE_WEB.Controllers
{
    public class BaseRoleController : Controller
    {
        private BaseRoleDao dao = new BaseRoleDao();
        // GET: BSRole
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }
        public ActionResult Setting(string ROLE_ID,string ROLE_NAME)
        {
            ViewBag.Id = ROLE_ID;
            ViewBag.Name = ROLE_NAME;
            return View();
        }

        public JsonResult GetTableData(string ROLE_ID, string ROLE_NAME, string ACT_FG)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(ROLE_ID, ROLE_NAME, ACT_FG), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSettingTableData(string ROLE_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetSettingTableData(ROLE_ID), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// 新增角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯角色資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 儲存編輯權限資料
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int SaveSetting(string ROLE_ID,List<SettingTable> data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.SaveSetting(ROLE_ID, data);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 複製權限資料
        /// </summary>
        /// <param name="ROLE_ID"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Copy(string ROLE_ID, Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Copy(ROLE_ID, form);
            }
            else
            {
                return -1;
            }
        }
    }
}
