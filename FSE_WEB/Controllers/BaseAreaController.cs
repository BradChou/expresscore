﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseAreaModel;

namespace FSE_WEB.Controllers
{
    public class BaseAreaController : Controller
    {
        private BaseAreaDao dao = new BaseAreaDao();
        // GET: BaseArea
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 取得縣市列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSect1Option()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetSect1Option(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得鄉鎮市區列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSect2Option(string AREA_SECT1)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetSect2Option(AREA_SECT1), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得街道路段列表
        /// </summary>
        /// <param name="AREA_SECT1"></param>
        /// <param name="AREA_SECT2"></param>
        /// <returns></returns>
        public JsonResult GetSect3Option(string AREA_SECT1,string AREA_SECT2)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetSect3Option(AREA_SECT1, AREA_SECT2), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得部門
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDeptOption()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetDeptOption(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTableData(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return new JsonResult()
                {
                    Data = dao.GetTableData(data),
                    MaxJsonLength = int.MaxValue,/*重點在這行*/
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// 新增配送區路段資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(data);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯配送區路段資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Edit(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(data);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 批次修改
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int BatchEdit(BatchForm data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.BatchEdit(data);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 批次刪除資料
        /// </summary>
        /// <param name="AREA_ID"></param>
        /// <returns></returns>
        public int Del(List<string> AREA_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Del(AREA_ID);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 匯入資料
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadFile(HttpPostedFileBase file)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.UploadFile(file);
            }
            else
            {
                return "Fail";
            }
        }

        /// <summary>
        /// 下載空白路段維護表
        /// </summary>
        /// <returns></returns>
        public FileResult DownloadFile()
        {
            FileStream stream = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DownloadFile", "空白路段維護表.xlsx"), FileMode.Open, FileAccess.Read, FileShare.Read);
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "空白路段維護表.xlsx");
        }
    }
}