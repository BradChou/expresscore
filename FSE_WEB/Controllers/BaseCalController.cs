﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseCalModel;

namespace FSE_WEB.Controllers
{
    public class BaseCalController : Controller
    {
        private BaseCalDao dao = new BaseCalDao();
        // GET: BaseCal
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得當月行事曆資料
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public JsonResult GetMainTableData(string DATE)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(DATE), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCopyTableData()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得行事曆資料(前一、目前、後一個月
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public JsonResult GetCalenderData(string Date)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetCalenderData(Date), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 新增行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除行事曆資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Delete(string CAL_SEQ)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Delete(CAL_SEQ);
            }
            else
            {
                return -1;
            }
        }

        public int Copy(string Year, bool Confirm, List<Form> data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                if (!dao.CheckYear(Year) || Confirm)
                {
                    return dao.Copy(Year, data);
                }
                else
                {
                    return -2;
                }
            }
            else
            {
                return -1;
            }
        }

    }
}
