﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.CustSetModel;

namespace FSE_WEB.Controllers
{
    public class CustSetController : Controller
    {
        CustSetDao dao = new CustSetDao();
        // GET: CustSet
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得下拉資料
        /// </summary>
        /// <param name="type"></param>
        /// <param name="condition">地址:代表City|銀行:代表Bank</param>
        /// <returns></returns>
        public JsonResult GetOption(string type,string condition)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetOption(type, condition), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得客戶資料表格資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetTableData(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(form), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得該客戶可用產品資料
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <returns></returns>
        public JsonResult GetProdTableData(string CUST_CODE)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetProdTableData(CUST_CODE), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得該客戶使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetUserTableData(FormUser form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetUserTableData(form), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得該客戶常用收送貨地址資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetAddrTableData(FormAddr form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetAddrTableData(form), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 新增客戶資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯客戶資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除客戶資料
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <returns></returns>
        public int Delete(string CUST_CODE)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Delete(CUST_CODE);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 新增該客戶可用產品
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddProd(FormProd form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.AddProd(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除該客戶可用產品
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <param name="PROD_ID"></param>
        /// <returns></returns>
        public int DeleteProd(string CUST_CODE,string PROD_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.DeleteProd(CUST_CODE, PROD_ID);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 新增該客戶使用者
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddUser(FormUser form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.AddUser(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯該客戶指定使用者
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int EditUser(FormUser form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.EditUser(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除該客戶指定使用者
        /// </summary>
        /// <param name="CUST_CODE"></param>
        /// <param name="USER_CODE"></param>
        /// <returns></returns>
        public int DeleteUser(string CUST_CODE, string USER_CODE)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.DeleteUser(CUST_CODE, USER_CODE);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 重置密碼
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int ResetPwd(FormUser form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.ResetPwd(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 新增常用地址
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int AddAddr(FormAddr form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.AddAddr(form);
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        /// 編輯常用地址
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int EditAddr(FormAddr form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.EditAddr(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除常用地址
        /// </summary>
        /// <param name="PS_SEQ"></param>
        /// <returns></returns>
        public int DeleteAddr(string PS_SEQ)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.DeleteAddr(PS_SEQ);
            }
            else
            {
                return -1;
            }
        }
    }
}
