﻿using FSE_WEB.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.LoginModel;

namespace FSE_WEB.Controllers
{
    public class LoginController : Controller
    {
        private LoginDao dao = new LoginDao();
        // GET: Login
        // 登入畫面
        public ActionResult Index()
        {
            ViewBag.ValidateCode = GetValidateCode();
            if (Session["Lang"] != null)
            {
                if (Session["Lang"].ToString().IndexOf("Chinese") >= 0)
                {
                    ViewBag.LangStr = "English";
                }
                else
                {
                    ViewBag.LangStr = "繁體中文";
                }
            }
            else
            {
                ViewBag.LangStr = "English";
                Session["Lang"] = "Chinese_Traditional";
            }
            return View();
        }

        /// <summary>
        /// 切換語言
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangeLang()
        {

            HttpCookie MyLang = new HttpCookie("MyLang");

            if (Session["Lang"].ToString().IndexOf("Chinese") >= 0)
            {
                MyLang.Value = "en-US";
                Response.Cookies.Add(MyLang);
                MyLang.Expires = DateTime.Now.AddDays(1);

                Session["Lang"] = "English";
            }
            else
            {
                MyLang.Value = null; //不指定語言預設為中文
                Response.Cookies.Add(MyLang);
                MyLang.Expires = DateTime.Now.AddDays(1);

                Session["Lang"] = "Chinese_Traditional";
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Login(string USER_ID,string USER_PWD)
        {
            return Json(dao.Login(USER_ID, USER_PWD));
        }

        /// <summary>
        /// 取得驗證碼
        /// </summary>
        /// <returns></returns>
        public string GetValidateCode()
        {
            int iFirst = 0;
            string ResultStr = String.Empty;
            int[] rtArray = new Int32[4];

            Random ro = new Random(4 * unchecked((int)DateTime.Now.Ticks));
            iFirst = ro.Next(49, 90);
            rtArray[0] = iFirst;

            for (int i = 1; i < 4; i++)
            {
                Random ri = new Random(i * iFirst * unchecked((int)DateTime.Now.Ticks));
                rtArray[i] = ri.Next(49, 90);
                if (rtArray[i] == 4)
                {
                    rtArray[i] = 8;
                }
                iFirst = rtArray[i];
            }

            for (int i = 0; i < 4; i++)
            {
                if (rtArray[i] > 57 && rtArray[i] < 65)
                {
                    rtArray[i] += 7;
                }
                ResultStr += ((char)rtArray[i]).ToString();
            }
            Session["ValidateCode"] = ResultStr;
            return ResultStr;
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Index");
        }

    }
}
