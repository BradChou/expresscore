﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseProdModel;

namespace FSE_WEB.Controllers
{
    public class BaseProdController : Controller
    {
        private BaseProdDao dao = new BaseProdDao();
        // GET: BaseProd
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult GetTableData(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(data), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail");
            }
        }

        /// <summary>
        /// 新增產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(data);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 修改產品資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Edit(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(data);
            }
            else
            {
                return -1;
            }
        }
    }
}