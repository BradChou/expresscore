﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseWordModel;

namespace FSE_WEB.Controllers
{
    public class BaseWordController : Controller
    {
        BaseWordDao dao = new BaseWordDao();
        // GET: BaseWord
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        public JsonResult GetHeaderTableData(string FUNC_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetHeaderTableData(FUNC_ID), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetWordTableData(string FUNC_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetWordTableData(FUNC_ID), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 新增字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        /// 編輯字典資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        /// 刪除字典資料
        /// </summary>
        /// <param name="WD_SEQ"></param>
        /// <param name="HEAD_FG"></param>
        /// <returns></returns>
        public int Delete(string WD_SEQ,string HEAD_FG)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Delete(WD_SEQ, HEAD_FG);
            }
            else
            {
                return -1;
            }
        }
    }
}
