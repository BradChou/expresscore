﻿using FSE_WEB.Dao;
using FSE_WEB.Models;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.HomeModel;

namespace FSE_WEB.Controllers
{
    public class HomeController : Controller
    {
        private HomeDao dao = new HomeDao();
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 取得樹狀選單畫面
        /// </summary>
        /// <returns></returns>
        public ActionResult Menu(string root,string type)
        {
            List<Menu> model = dao.GetMenu(root,type);
            return PartialView(model);
        }

        /// <summary>
        /// 取得樹狀選單JSON資料
        /// </summary>
        /// <returns></returns>
        public JsonResult MenuJson(string root,string type)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetMenu(root, type), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
    }
}