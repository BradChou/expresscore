﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.SysFuncModel;

namespace FSE_WEB.Controllers
{
    public class SysFuncController : Controller
    {
        private SysFuncDao dao = new SysFuncDao();
        // GET: SysFunc
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得搜尋資料
        /// </summary>
        /// <param name="FUNC_ID"></param>
        /// <param name="FUNC_DESC"></param>
        /// <returns></returns>
        public JsonResult GetTableData(string FUNC_ID, string FUNC_DESC)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(FUNC_ID, FUNC_DESC), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// 新增系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯系統功能
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }
    }
}