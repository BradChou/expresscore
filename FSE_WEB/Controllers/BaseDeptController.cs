﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BaseDeptModel;

namespace FSE_WEB.Controllers
{
    public class BaseDeptController : Controller
    {
        private BaseDeptDao dao = new BaseDeptDao();
        // GET: BaseDept
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }
        
        /// <summary>
        /// 取得下拉選項
        /// </summary>
        /// <param name="FUNC_ID"></param>
        /// <returns></returns>
        public JsonResult GetOption(string FUNC_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetOption(FUNC_ID), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得部門資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult GetTableData(Form data)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(data), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 新增部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 修改部門資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }
    }
}