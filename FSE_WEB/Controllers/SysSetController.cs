﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.SysSetModel;

namespace FSE_WEB.Controllers
{
    public class SysSetController : Controller
    {
        SysSetDao dao = new SysSetDao();
        // GET: SysSet
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }
        public JsonResult GetTableData(string SET_DESC)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(SET_DESC), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// 新增系統參數
        /// </summary>
        /// <param name="form"></param>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 編輯系統參數
        /// </summary>
        /// <param name="form"></param>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }

    }
}
