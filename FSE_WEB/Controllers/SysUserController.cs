﻿using FSE_WEB.Dao;
using FSE_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static FSE_WEB.Models.BasicModel;
using static FSE_WEB.Models.SysUserModel;

namespace FSE_WEB.Controllers
{
    public class SysUserController : Controller
    {
        private SysUserDao dao = new SysUserDao();
        // GET: SysUser
        public ActionResult Index()
        {
            ViewBag.Auth = dao.GetAuth(Request.FilePath);
            return View();
        }

        /// <summary>
        /// 取得部門選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDeptOption()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetDeptOption(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// 取得職位選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetJobTitleOption()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetJobTitleOption(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// 取得角色選項
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRoleOption()
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetRoleOption(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 取得使用者列表
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetTableData(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return Json(dao.GetTableData(form), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Fail");
            }
        }

        /// <summary>
        /// 新增使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Add(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Add(form);
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        /// 修改使用者資料
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public int Edit(Form form)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Edit(form);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 刪除使用者資料
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <returns></returns>
        public int Del(string USER_ID)
        {
            if (TokenUtil.VerifyToken(Request.Headers.GetValues("accessToken")))
            {
                return dao.Del(USER_ID);
            }
            else
            {
                return -1;
            }
        }
    }
}