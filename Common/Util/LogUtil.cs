﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Common.Util
{
    public class LogUtil
    {
        /// <summary>
        /// 將錯誤訊息寫入~/LogReport.txt
        /// </summary>
        /// <param name="str">欲寫入錯誤訊息</param>
        public static void ErrorLog(string str)
        {
            string showString = "";
            StackTrace ss = new StackTrace(true);
            //取得呼叫當前方法之上一層類別(GetFrame(1))的屬性
            MethodBase mb = ss.GetFrame(1).GetMethod();

            //取得呼叫當前方法之上一層類別(父方)的命名空間名稱
            showString += mb.DeclaringType.Namespace + "/";

            //取得呼叫當前方法之上一層類別(父方)的function 所屬class Name
            showString += mb.DeclaringType.Name + "/";

            //取得呼叫當前方法之上一層類別(父方)的Function Name
            showString += mb.Name + "/";

            using (StreamWriter sw = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "/LogReport.txt"))   //小寫TXT     
            {
                // Add some text to the file.
                sw.Write(DateTime.Now);
                sw.Write(showString);
                sw.WriteLine("  [Error Message] ");
                sw.WriteLine("-------------------------------------------------------------------");
                sw.WriteLine(" Error Message  : " + str);
                sw.WriteLine("-------------------------------------------------------------------");
            }
        }
    }
}