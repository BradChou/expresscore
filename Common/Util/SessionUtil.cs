﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Util
{
    public class SessionUtil
    {
        // private constructor
        private SessionUtil()
        {
        }

        // Gets the current session.
        public static SessionUtil Current
        {
            get
            {
                SessionUtil session =
                  (SessionUtil)HttpContext.Current.Session["__MySession__"];
                if (session == null)
                {
                    session = new SessionUtil();
                    HttpContext.Current.Session["__MySession__"] = session;
                }
                return session;
            }
        }

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string RoleId { get; set; }
    }
}