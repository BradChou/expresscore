﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BAL.Model.ReturnOrder
{
    public class ReturnAddModel
    {

        public static List<SelectListItem> GetPaymentMethodSelectList()
        {
            return new List<SelectListItem>{
                    new SelectListItem { Text = "月結", Value = "1" },
                    new SelectListItem { Text = "預購", Value = "2" }
            };
        }
        public static List<SelectListItem> GeTimePeriodSelectList()
        {
            return new List<SelectListItem>{
                    new SelectListItem { Text = "不指定", Value = "0" },
                    new SelectListItem { Text = "早", Value = "1" },
                    new SelectListItem { Text = "中", Value = "2" },
                    new SelectListItem { Text = "晚", Value = "3" }
            };

        }
        public static List<SelectListItem> GeDeliveryMethodSelectList()
        {
            return new List<SelectListItem>{
                    new SelectListItem { Text = "正物流(送貨)", Value = "1" },
                    new SelectListItem { Text = "逆物流(退貨)", Value = "2" },
                    new SelectListItem { Text = "來回件(退換貨)", Value = "3" }
            };
        }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 客戶編號
        /// </summary>
        public string CustomerId { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string ReceiveContact { get; set; }
        /// <summary>
        /// 收件人電話1
        /// </summary>
        public string ReceiveTel1 { get; set; }
        /// <summary>
        /// 收件人電話2
        /// </summary>
        public string ReceiveTel2 { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ReceiveAddress { get; set; }
        /// <summary>
        /// 寄件人
        /// </summary>
        public string SendContact { get; set; }
        /// <summary>
        /// 寄件人電話1
        /// </summary>
        public string SendTel1 { get; set; }
        /// <summary>
        /// 寄件人電話2
        /// </summary>
        public string SendTel2 { get; set; }
        /// <summary>
        /// 寄件人地址
        /// </summary>
        public string SendAddress { get; set; }
        /// <summary>
        /// 付款方式(1=月結 2=預購)
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 代收貨款
        /// </summary>
        public int? CollectionMoney { get; set; }
        /// <summary>
        /// 預計抵達日
        /// </summary>
        public string Arriveassigndate { get; set; }
        /// <summary>
        /// 派送時段(0=不指定 1=早 2=中 3=晚)
        /// </summary>
        public string TimePeriod { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        public string InvoiceDesc { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
        public string ArticleNumber { get; set; }
        /// <summary>
        /// 出貨平台
        /// </summary>
        public string SendPlatform { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        public string ArticleName { get; set; }
        /// <summary>
        /// 訂單類別(1=正物流(送貨) 2=逆物流(退貨) 3=來回件(退換貨)
        /// </summary>
        public string DeliveryMethod { get; set; }
        /// <summary>
        /// 客戶收據收回
        /// </summary>
        public bool ReceiptFlag { get; set; }
        /// <summary>
        /// 貨物
        /// </summary>
        public List<OrderGood> OrderGoods { get; set; }

    }

}



