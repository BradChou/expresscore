﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Model.BaseOrder
{
   public class OrderBatchUploadModel
    {
        public string ExcelImport { get; set; }
        
        public int SuccessCount { get; set; }
        public int FailCount { get; set; }
        public string ErrorMessage { get; set; }
    }
}
