﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static DAL.Model.BasicModel;

namespace BAL.Model.BaseOrder
{
    public class OrderAddModel
    {
       
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 客戶編號
        /// </summary>
        public string CustomerId { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        [Required(ErrorMessage = "收件人沒填")]
        [MaxLength(20, ErrorMessage = "收件人最大長度20")]
        public string ReceiveContact { get; set; }
        /// <summary>
        /// 收件人電話1
        /// </summary>
        [Required(ErrorMessage = "收件人電話1沒填")]
        [MaxLength(20, ErrorMessage = "收件人電話1最大長度20")]
        public string ReceiveTel1 { get; set; }
        /// <summary>
        /// 收件人電話2
        /// </summary>
        public string ReceiveTel2 { get; set; }
        /// <summary>
        /// 收件人城市
        /// </summary>
        [Required(ErrorMessage = "收件人城市沒填")]
        public string ReceiveCity { get; set; }
        /// <summary>
        /// 收件人地區
        /// </summary>
        [Required(ErrorMessage = "收件人地區沒填")]
        public string ReceiveArea { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        [Required(ErrorMessage = "收件人地址沒填")]
        [MaxLength(300, ErrorMessage = "收件人地址最大長度300")]
        public string ReceiveAddress { get; set; }
        /// <summary>
        /// 寄件人
        /// </summary>
        [Required(ErrorMessage = "寄件人沒填")]
        [MaxLength(20, ErrorMessage = "寄件人最大長度20")]
        public string SendContact { get; set; }
        /// <summary>
        /// 寄件人電話1
        /// </summary>
        [Required(ErrorMessage = "寄件人電話1沒填")]
        [MaxLength(20, ErrorMessage = "寄件人電話1最大長度20")]
        public string SendTel1 { get; set; }
        /// <summary>
        /// 寄件人電話2
        /// </summary>
        public string SendTel2 { get; set; }
        /// <summary>
        /// 收件人城市
        /// </summary>
        [Required(ErrorMessage = "收件人城市沒填")]
        public string SendCity { get; set; }
        /// <summary>
        /// 收件人地區
        /// </summary>
        [Required(ErrorMessage = "收件人地區沒填")]
        public string SendArea { get; set; }
        /// <summary>
        /// 寄件人地址
        /// </summary>
        [Required(ErrorMessage = "寄件人地址沒填")]
        [MaxLength(300, ErrorMessage = "寄件人地址最大長度300")]
        public string SendAddress { get; set; }
        /// <summary>
        /// 付款方式(1=月結 2=預購)
        /// </summary>
        [Required(ErrorMessage = "付款方式沒填")]
        public int? PaymentMethod { get; set; }
        /// <summary>
        /// 代收貨款
        /// </summary>
        [Range(0, 20000)]
        [Display(Name = "代收貨款")]
        public int? CollectionMoney { get; set; }
        /// <summary>
        /// 預計抵達日
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? Arriveassigndate { get; set; }
        /// <summary>
        /// 派送時段(0=不指定 1=早 2=中 3=晚)
        /// </summary>
        [Required(ErrorMessage = "派送時段沒填")]
        public int? TimePeriod { get; set; }
        /// <summary>
        /// 備註
        /// </summary>
        [MaxLength(200, ErrorMessage = "備註最大長度200")]
        public string InvoiceDesc { get; set; }
        /// <summary>
        /// 產品編號
        /// </summary>
         [MaxLength(20, ErrorMessage = "產品編號最大長度20")]
        public string ArticleNumber { get; set; }
        /// <summary>
        /// 出貨平台
        /// </summary>
        [MaxLength(20, ErrorMessage = "出貨平台最大長度20")]
        public string SendPlatform { get; set; }
        /// <summary>
        /// 品名
        /// </summary>
        [MaxLength(20, ErrorMessage = "品名最大長度20")]
        public string ArticleName { get; set; }
        /// <summary>
        /// 訂單類別(1=正物流(送貨) 2=逆物流(退貨) 3=來回件(退換貨)
        /// </summary>
        [Required(ErrorMessage = "訂單類別沒填")]
        public int? DeliveryMethod { get; set; }
        /// <summary>
        /// 客戶收據收回
        /// </summary>
        public bool ReceiptFlag { get; set; }
        /// <summary>
        /// 貨物
        /// </summary>
        [Required(ErrorMessage = "至少一筆貨")]
        public List<OrderGood> OrderGoods { get; set; }
        /// <summary>
        /// 批次匯入Excel
        /// </summary>

        public string ExcelImport { get; set; }
        

    }

}


public class OrderGood
{

    /// <summary>
    /// 尺寸(三邊和(cm)1=S60 2=S90 3=S120 4=S150 5=S210 6=S110 99=袋裝)
    /// </summary>
    public int? CbmSize { get; set; }
   // public string Lengh { get; set; }

}

