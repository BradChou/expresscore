﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BAL.Model.BaseOrder
{
    public class OrderSearchTable
	{


		public long id { get; set; }
		public string OrderId { get; set; }
		public int? Quantity { get; set; }
		public string ReceiveContact { get; set; }
		public string ReceiveTel1 { get; set; }

		public string ReceiveAddress { get; set; }
		public string SendContact { get; set; }
		public string SendTel1 { get; set; }

		public string SendAddress { get; set; }
		public string PaymentMethod { get; set; }
		public int CollectionMoney { get; set; }
		public string Arriveassigndate { get; set; }
		public string TimePeriod { get; set; }

		public string DeliveryMethod { get; set; }
	
		public string ExcelImport { get; set; }
		public string Status { get; set; }
		public string CreateDate { get; set; }
		public string UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }


	}

}



