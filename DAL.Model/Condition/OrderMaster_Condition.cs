﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
   public  class OrderMaster_Condition
    {
		public long id { get; set; }
		public string OrderId { get; set; }
		public int? Quantity { get; set; }
		public string ReceiveContact { get; set; }
		public string ReceiveTel1 { get; set; }
		public string ReceiveTel2 { get; set; }
		public string ReceiveCity { get; set; }
		public string ReceiveArea { get; set; }
		public string ReceiveAddress { get; set; }
		public string SendContact { get; set; }
		public string SendTel1 { get; set; }
		public string SendTel2 { get; set; }
		public string SendCity { get; set; }
		public string SendArea { get; set; }
		public string SendAddress { get; set; }
		public int PaymentMethod { get; set; }
		public int CollectionMoney { get; set; }
		public DateTime? Arriveassigndate { get; set; }
		public int TimePeriod { get; set; }
		public string InvoiceDesc { get; set; }
		public string ArticleNumber { get; set; }
		public string SendPlatform { get; set; }
		public string ArticleName { get; set; }
		public int DeliveryMethod { get; set; }
		public bool ReceiptFlag { get; set; }
		public string ExcelImport { get; set; }
		public string Status { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }
	}
}
