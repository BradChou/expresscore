﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
  public class BS_AREA_Condition
    {
		public int AREA_ID { get; set; }
		public string AREA_SECT1 { get; set; }
		public string AREA_SECT2 { get; set; }
		public string AREA_SECT3 { get; set; }
		public string AREA_SECT4 { get; set; }
		public string DEPT_ID { get; set; }
		public string DEPT_NAME { get; set; }
		public string ZIP_CODE { get; set; }
		public string ZIP3 { get; set; }
		public string SD_CODE { get; set; }
		public string MD_CODE { get; set; }
		public string STACK_AREA { get; set; }
		public string REMOTE_FG { get; set; }
		public string TRSF_FG { get; set; }
		public string CREATE_USER { get; set; }
		public DateTime? CREATE_TIME { get; set; }
		public string EDIT_USER { get; set; }
		public DateTime? EDIT_TIME { get; set; }
	}
}
