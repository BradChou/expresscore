﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model.Condition
{
    public class OrderMasterGoods_Condition
    {
		public long id { get; set; }
		public string OrderId { get; set; }
		public int CbmSize { get; set; }
		public int? Length { get; set; }
		public int? Width { get; set; }
		public int? Height { get; set; }
		public int? Weight { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }
	}
}
