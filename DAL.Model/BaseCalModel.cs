﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class BaseCalModel
    {
        public class Table
        {
            /// <summary>
            /// tabulator ID
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 系統序號
            /// </summary>
            public int CAL_SEQ { get; set; }
            /// <summary>
            /// 日期(日)
            /// </summary>
            public int DAY { get; set; }
            /// <summary>
            /// 日期
            /// </summary>
            public string CAL_DATE { get; set; }
            /// <summary>
            /// 說明
            /// </summary>
            public string CAL_DESC { get; set; }
            /// <summary>
            /// 休假否
            /// </summary>
            public string REST_FG { get; set; }
            /// <summary>
            /// 類別
            /// </summary>
            public string FIX_FG { get; set; }
            /// <summary>
            /// 備註
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 狀態(啟用/停用)
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 勾選
            /// </summary>
            public bool CK_FG { get; set; }
        }

        public class Form
        {
            /// <summary>
            /// 系統序號
            /// </summary>
            public int CAL_SEQ { get; set; }
            /// <summary>
            /// 日期
            /// </summary>
            public string CAL_DATE { get; set; }
            /// <summary>
            /// 說明
            /// </summary>
            public string CAL_DESC { get; set; }
            /// <summary>
            /// 休假否
            /// </summary>
            public string REST_FG { get; set; }
            /// <summary>
            /// 類別
            /// </summary>
            public string FIX_FG { get; set; }
            /// <summary>
            /// 備註
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 狀態(啟用/停用)
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 勾選
            /// </summary>
            public bool CK_FG { get; set; }
        }
        public class Calender
        {
            public string[] months { get; set; }
            public string[] days { get; set; }
            public string desc { get; set; }
        }
    }
}