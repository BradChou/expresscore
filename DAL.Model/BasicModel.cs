﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class BasicModel
    {
        public class Option
        {
            public string value { get; set; }
            public string label { get; set; }
        }
    }
}