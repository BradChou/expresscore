﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class BaseDeptModel
    {
        public class SearchForm
        {
            /// <summary>
            /// 部門代碼
            /// </summary>
            public string id { get; set; }
            /// <summary>
            /// 部門名稱
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 類別
            /// </summary>
            public string typeValue { get; set; }
            /// <summary>
            /// 部門層級
            /// </summary>
            public string levelValue { get; set; }
            /// <summary>
            /// 轉運站
            /// </summary>
            public string stnValue { get; set; }
            /// <summary>
            /// 區域
            /// </summary>
            public string areaValue { get; set; }
            /// <summary>
            /// 使用狀態
            /// </summary>
            public string actValue { get; set; }
        }
        
        public class Form
        {
            /// <summary>
            /// 部門代碼
            /// </summary>
            public string DEPT_ID { get; set; }
            /// <summary>
            /// 部門名稱
            /// </summary>
            public string DEPT_NAME { get; set; }
            /// <summary>
            /// 部門簡稱
            /// </summary>
            public string DEPT_BRF { get; set; }
            /// <summary>
            /// 類別
            /// </summary>
            public string DEPT_TYPE { get; set; }
            /// <summary>
            /// 電話
            /// </summary>
            public string TEL_NO { get; set; }
            /// <summary>
            /// 聯絡人
            /// </summary>
            public string CONTACT { get; set; }
            /// <summary>
            /// 地址
            /// </summary>
            public string DEPT_ADDR { get; set; }
            /// <summary>
            /// 經度
            /// </summary>
            public string POS_LONG { get; set; }
            /// <summary>
            /// 緯度
            /// </summary>
            public string POS_LATI { get; set; }
            /// <summary>
            /// 部門層級
            /// </summary>
            public string DEPT_LEVEL { get; set; }
            /// <summary>
            /// 轉運站
            /// </summary>
            public string DEPT_STN { get; set; }
            /// <summary>
            /// 所屬大區域
            /// </summary>
            public string B_AREA { get; set; }
            /// <summary>
            /// 區域
            /// </summary>
            public string AREA { get; set; }
            /// <summary>
            /// 啟用/停用
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 新增者工號
            /// </summary>
            public string CREATE_USER { get; set; }
            /// <summary>
            /// 編輯者工號
            /// </summary>
            public string EDIT_USER { get; set; }
        }
    }
}