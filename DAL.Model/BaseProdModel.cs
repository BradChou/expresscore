﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class BaseProdModel
    {
        //搜尋與列表欄位與新增修改幾乎重複所以用同一個物件
        public class Form
        {
            /// <summary>
            /// 產品代碼
            /// </summary>
            public string PROD_ID { get; set; }
            /// <summary>
            /// 產品名稱
            /// </summary>
            public string PROD_NAME { get; set; }
            /// <summary>
            /// 產品性質
            /// </summary>
            public string PROD_TYPE { get; set; }
            /// <summary>
            /// 狀態
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 創建者
            /// </summary>
            public string CREATE_USER { get; set; }
            /// <summary>
            /// 編輯者
            /// </summary>
            public string EDIT_USER { get; set; }
        }
    }
}