﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class SysFuncModel
    {
        public class Table
        {
            /// <summary>
            /// tabulator ID
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 功能編號
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能名稱
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 上層編號
            /// </summary>
            public string PARENT_ID { get; set; }
            /// <summary>
            /// 功能類別
            /// </summary>
            public string FUNC_TYPE { get; set; }
            /// <summary>
            /// 程序位置
            /// </summary>
            public string URL_ADDR { get; set; }
            /// <summary>
            /// 備註事項
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 設定可執行否      
            /// </summary>
            public string SET_RUN { get; set; }
            /// <summary>
            /// 設定可查詢否
            /// </summary>
            public string SET_SEARCH { get; set; }
            /// <summary>
            /// 設定可新增否
            /// </summary>
            public string SET_ADD { get; set; }
            /// <summary>
            /// 設定可修改否
            /// </summary>
            public string SET_MODIFY { get; set; }
            /// <summary>
            /// 設定可刪除否
            /// </summary>
            public string SET_DEL { get; set; }
            /// <summary>
            /// 設定可列印否
            /// </summary>
            public string SET_PRINT { get; set; }
        }
        public class Form
        {
            /// <summary>
            /// 功能編號
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能名稱
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 上層編號
            /// </summary>
            public string PARENT_ID { get; set; }
            /// <summary>
            /// 功能類別
            /// </summary>
            public string FUNC_TYPE { get; set; }
            /// <summary>
            /// 程序位置
            /// </summary>
            public string URL_ADDR { get; set; }
            /// <summary>
            /// 備註事項
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 設定可執行否      
            /// </summary>
            public string SET_RUN { get; set; }
            /// <summary>
            /// 設定可查詢否
            /// </summary>
            public string SET_SEARCH { get; set; }
            /// <summary>
            /// 設定可新增否
            /// </summary>
            public string SET_ADD { get; set; }
            /// <summary>
            /// 設定可修改否
            /// </summary>
            public string SET_MODIFY { get; set; }
            /// <summary>
            /// 設定可刪除否
            /// </summary>
            public string SET_DEL { get; set; }
            /// <summary>
            /// 設定可列印否
            /// </summary>
            public string SET_PRINT { get; set; }
        }
    }
}