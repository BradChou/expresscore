﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class OrderSearchModel
    {
        public class SearchForm
        {
         
            public string Status { get; set; }

            public string DeliveryMethod { get; set; }

            public string OrderId { get; set; }

            public DateTime? StartDate { get; set; }

            public DateTime? EndDate { get; set; }

            public string DeliveryId { get; set; }


        }
        public class Form
        {
            public string OrderId { get; set; }
            public string CreateDate { get; set; }
            public string SendContact { get; set; }
            public string ReceiveContact { get; set; }
            public string ReceiveAddress { get; set; }
            public int Quantity { get; set; }
            public int CollectionMoney { get; set; }
            public string Arriveassigndate { get; set; }
            public string Status { get; set; }
            public string DeliveryId { get; set; }

            public string UpdateUser { get; set; }
        }
    }
}