﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class SysUserModel
    {
        public class SearchForm
        {
            /// <summary>
            /// 員工編號
            /// </summary>
            public string id { get; set; }
            /// <summary>
            /// 員工姓名
            /// </summary>
            public string name { get; set; }
            /// <summary>
            /// 所屬部門
            /// </summary>
            public string dept { get; set; }
            /// <summary>
            /// 類別
            /// </summary>
            public string type { get; set; }
            /// <summary>
            /// 啟用狀態
            /// </summary>
            public string act { get; set; }
        }
        public class Form
        {
            /// <summary>
            /// 員工編號
            /// </summary>
            public string USER_ID { get; set; }
            /// <summary>
            /// 員工姓名
            /// </summary>
            public string USER_NAME { get; set; }
            /// <summary>
            /// 使用者類別
            /// </summary>
            public string USER_TYPE { get; set; }
            /// <summary>
            /// 密碼
            /// </summary>
            public string USER_PWD { get; set; }
            /// <summary>
            /// 所屬部門
            /// </summary>
            public string DEPART_ID { get; set; }
            /// <summary>
            /// 職位
            /// </summary>
            public string JOB_TITLE { get; set; }
            /// <summary>
            /// 性別
            /// </summary>
            public string USER_SEX { get; set; }
            /// <summary>
            /// 電話
            /// </summary>
            public string TEL_NO { get; set; }
            /// <summary>
            /// 手機
            /// </summary>
            public string CELL_PHONE { get; set; }
            /// <summary>
            /// 載運類別
            /// </summary>
            public string TRANS_TYPE { get; set; }
            /// <summary>
            /// 載運編號
            /// </summary>
            public string TRANS_CODE { get; set; }
            public string EMAIL { get; set; }
            /// <summary>
            /// 角色
            /// </summary>
            public string ROLE_ID { get; set; }
            /// <summary>
            /// 啟用狀態
            /// </summary>
            public string ACT_FG { get; set; }
            /// <summary>
            /// 啟用日期
            /// </summary>
            public string ACT_DATE { get; set; }
            /// <summary>
            /// 離職日期
            /// </summary>
            public string VOID_DATE { get; set; }
            /// <summary>
            /// 密碼到期日
            /// </summary>
            public string EXPIRE_DATE { get; set; }
            /// <summary>
            /// 備註
            /// </summary>
            public string REMARKS { get; set; }
            /// <summary>
            /// 創建者
            /// </summary>
            public string CREATE_USER { get; set; }
            /// <summary>
            /// 修改者
            /// </summary>
            public string EDIT_USER { get; set; }
        }
    }
}