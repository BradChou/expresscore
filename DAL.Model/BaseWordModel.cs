﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Model
{
    public class BaseWordModel
    {
        public class Table
        {
            /// <summary>
            /// 系統編號
            /// </summary>
            public int WD_SEQ { get; set; }
            /// <summary>
            /// 功能代碼
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能說明
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 詞彙資料
            /// </summary>
            public string CMN_WORD { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
            /// <summary>
            /// 項目Header(Y/非Y)
            /// </summary>
            public string HEAD_FG { get; set; }
        }
        public class Form
        {
            /// <summary>
            /// 系統編號
            /// </summary>
            public int WD_SEQ { get; set; }
            /// <summary>
            /// 功能代碼
            /// </summary>
            public string FUNC_ID { get; set; }
            /// <summary>
            /// 功能說明
            /// </summary>
            public string FUNC_DESC { get; set; }
            /// <summary>
            /// 詞彙資料
            /// </summary>
            public string CMN_WORD { get; set; }
            /// <summary>
            /// 顯示順序
            /// </summary>
            public string DSP_ORD { get; set; }
            /// <summary>
            /// 項目Header(Y/非Y)
            /// </summary>
            public string HEAD_FG { get; set; }
        }
    }
}